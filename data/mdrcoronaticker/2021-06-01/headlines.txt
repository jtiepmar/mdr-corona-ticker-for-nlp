###21:30 | tickerende
###19:44 | auch jugendliche ab 14 jahren k önnen geimpft werden
###19:30 | rki senkt corona-risikoeinstufung für deutschland auf hoch
###19:14 | pandemie-effekt: schwächste jemals registrierte grippewelle
###17:55 | 26 weitere todesfälle
###17:35 | dresdner uniklinikum und fachkliniken radeburg versorgen ältere menschen nach infektion
###16:50 | mit dem schnelltest ins freibad
###16:34 | weiterer klärungsbedarf bei impfterminen für jugendliche
###16:15 | mackenroth: kinder mit migrationshintergrund mehr unterstützen
###15:55 | sachsen ebnet weg für sonderfonds des bundes
###15:40 | neue corona-verordnung in thüringen
###15:25 | herdenimmunität rückt in greifbare nähe
###15:00 | schostakowitsch-tage ziehen um
###14:40 | neue einreisebestimmungen nach tschechien
###14:20 | lehrerverband besorgt um kinderwohl
###14:05 | leipzig: medientage starten
###13:40 | dresdner zoo öffnet tierhäuser
###13:12 | staatsschauspiel beginnt mit ersten stücken vor publikum
###12:40 | spanische ärztin erhält dresden-preis mit verspätung
###12:02 | stadtmuseum riesa lässt besucher über eintrittspreis entscheiden
###11:43 | görlitz-information hat wieder geöffnet
###11:31 | dresden ermöglicht ab heute schwimmunterricht
###10:55 | weiße flotte startet mitte juni mit ausflugsfahrten
###10:44 | wieder regelunterricht im landkreis meißen
###10:30 | swiss will wieder zwischen dresden und zürich fliegen
###10:00 | dresdner musikfestspiele vor publikum
###09:11 | leipzig fördert ab sofort kulturelle kleinprojekte
###08:40 | urlaub vielerorts möglich - kaum aussichten auf schnäppchen
###08:25 | familienverbände fordern: lobby für kinder stärken
###07:20 | görlitzer parkeisenbahn nimmt wieder den betrieb auf
###07:00 | hunderte gegner der corona-beschränkungen ziehen durch freiberg
###06:30 | sieben-tage-inzidenz im freistaat nahezu unverändert
###06:05 | zwönitzer setzen zeichen für friedliche stadt
###05:50 | virus-varianten werden umgetauft
###05:45 | schmerzpatienten gehen weniger zum arzt
###05:28 | dresdner philharmonie begeistert publikum mit streichmusik
###05:09 | innengastronomie in leipzig und im vogtland nachgefragt
