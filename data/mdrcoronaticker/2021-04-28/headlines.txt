###20:50 | tickerende
###20:33 | gastgewerbe findet keinen nachwuchs
###20:11 | corona-news aus deutschland und der welt
###20:00 | vogler: kultur hat keinen kompletten lockdown verdient
###19:27 | härtefallprogramm für unternehmen kommt
###18:38 | landesverfassungsschutz prüft einzelfallbeobachtung von querdenkern
###18:11 | weniger neuinfektionen als vor einer woche
###17:21 | landesärztekammer: astrazeneca unverzichtbar für impfkampagne
###17:02 | dresdner musikfestspiele werden geteilt und verlegt
###16:39 | landesjugendpfarrer fordert pespektiven für jugend
###16:08 | drk: schließung der impfzentren verfrüht
###15:41 | theater plauen-zwickau setzt auf freiluftevents
###15:07 | streit um corona-politik: augustusburger bürgermeister verlässt spd
###14:50 | verbaucher wollen weniger geld ausgeben
###14:29 | zoo-außenanlagen weiter geöffnet
###14:04 | pauls will an der elbe theater machen
###13:50 | leipziger galeristin berichtet über online-konferenz mit der bundeskanzlerin
###13:25 | konjunktureinbruch drückt strom- und gasabsatz bei envia m
###12:46 | regionaler nachtbusverkehr des vvo pausiert ab freitag
###12:04 | homeoffice mit kaffee-flatrate im zoo hoyerswerda
###11:40 | querdenker im blick des bundesverfassungsschutzes
###11:12 | forscher beurteilt impfungen mit verschiedenen stoffen
###10:53 | landratsamt im vogtland: corona-maßnahmen wirken
###10:19 | zahl der neuinfektionen leicht gesunken
###09:56 | trotz umstellung auf maskenproduktion kurzarbeit bei damino
###09:20 | impfung für odachlose
###08:41 | neues testzentrum in bad schlema in betrieb
###08:09 | belegschaft der gläsernen manufaktur streikt zu hause
###07:31 | mehr rechte für geimpfte ab 10.mai
###07:22 | drk sachsen begrüßt planungssicherheit für impfzentren
###06:47 | radeburg sagt verleihung des zille-karikaturenpreises ab
###06:15 | diskussion über rechte für geimpfte
###05:54 | ffp2-maskenpflicht in bussen und straßenbahnen
###05:28 | millionste dosis wird in impfzentren verabreicht
###05:17 | nachgeholte auszeichnung
###05:08 | chemnitz will kultur wiederbeleben
