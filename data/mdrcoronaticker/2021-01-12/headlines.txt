###20:20 | tickerende
###19:56 | viele pflegekräfte in mitteldeutschland impfwillig
###19:22 | alpine skisaison in sachsen beendet
###18:48 | vorerst keine impftermine per telefon
###18:30 | knapp 2.000 neuinfektionen in sachsen
###18:15 | sachsen gegen impfpflicht für pflegekräfte
###17:51 | vogtlandkreis veröffentlicht versehentlich sensible daten
###17:22 | kretschmer stellt große lockerungen ab februar infrage
###16:53 | bessere luft in sachsen dank corona.
###16:11 | books to go in bibliothek rochlitz
###15:53 | schwer erkrankter ehv-trainer auf weg der besserung
###15:16 | corona-bundeswehreinsatz im kreis görlitz verlängert
###14:59 | drk: keine pannen bei impfportal-start
###14:38 | pilotprojekt für virenfreie luft in schulen
###14:23 | weiter it-probleme bei corona-fallbearbeitung im vogtland
###13:57 | ffp2-maskenpflicht in pflegeheimen
###12:51 | bestatterinnung: lage in krematorien unter kontrolle
###11:36 | dgb: auch bei unternehmern corona-leugner
###11:20 | ihk dresden warnt vor lockdown der gesamten wirtschaft
###11:10 | fliegender sessel der tu chemnitz ändert das konzept
###10:50 | astra-zeneca-impfstoff: zulassung in eu beantragt
###10:40 | corona-dikatur ist eines der unwörter des jahres
###10:21 | wissensvideos fürs homeschooling in der ard-mediathek
###09:44 | schneeberg lässt weihnachtsbeleuchtung erstmals bis mariä lichtmess
###09:30 | sparkasse meißen besetzt zehn filialen nicht mehr mit personal
###09:02 | elternrat aus kreis leipzig fordert entschlackten lernplan
###08:04 | polizei will nicht ohne anlass in wohnungen kontrollieren
###07:42 | anmeldungsseite für impftermine ruckelt weiterhin
###07:00 | treuen fordert reaktivierung des haltepunktes eich für impfzentrum
###06:07 | termin beim juristen ist triftiger grund zum rausgehen
###05:56 | oma und opa dürfen fremdes kind mitbetreuen
###05:33 | sachsen erhält ersten moderna-impfstoff
###05:16 | 15-kilometer-regel gilt auch für nicht-sachsen
###05:00 | wenige verstöße gegen corona-vorschriften in sächsischen firmen
