###19:38 | tickerende
###19:05 | 45 anzeigen nach demonstration in chemnitz
###18:52 | 1.101 neue corona-infektionen in sachsen
###18:32 | spahn: lockdown muss nach ostern weitergehen
###18:18 | flaschen auf beamte geworfen
###17:14 | auch in dresden demonstrierende auf der straße
###16:55 | chefarzt von zittauer corona-station stirbt an corona
###15:32 | 200 menschen demonstrieren in zwickau
###15:02 | vogtländische ärzte richten impfappell an mitmenschen
###14:55 | wöller: infektionsschutz ist nicht immer durchsetzbar
###14:36 | kleinere gruppe versammelt sich auf chemnitzer marktplatz
###14:19 | neues chemnitzer impfzentrum nimmt den betrieb auf
###13:45 | dresden verschärft corona-schutz, aber läden sollen offen bleiben
###13:18 | eine milliarde euro für lernschwache schüler
###12:55 | wocheninzidenz für sachsen steigt auf 176
###12:28 | ovg bestätigt demoverbot in chemnitz
###12:12 | spahn beantwortet im internet fragen zu corona
###12:02 | pandemie verändert unser verhalten
###11:32 | verhaltener optimismus bei virologen
###11:18 | weiter erleichterungen für sonntagsarbeit und überstunden
###10:11 | sachsens krankenhäuser in wirtschaftlicher not
###09:41 | corona-notstand in tschechien verlängert
###09:14 | demos in chemnitz und schwarzenberg untersagt
###08:56 | infektionszahlen in sachsen steigen weiter
###08:21 | polen verschärft lockdown weiter
###08:00 | online-preisverleihung für junge forscher
###07:45 | diskussion um astrazeneca bremst impffortschritt in sachsen
###07:35 | besuchereinbruch in sachsens schlössern und parks
###07:12 | nur online dabei - beisetzungen in corona-zeiten
