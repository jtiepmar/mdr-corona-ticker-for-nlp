###20:01 | tickerende
###19:52 | berlins bürgermeister müller widerspricht kretschmer
###19:40 | corona-konformer karneval in wittichenau
###19:23 | faschingsfeier trotz corona-beschränkungen auf radeburger marktplatz
###18:48 | hotelgewerbe kritisiert aussagen von kretschmer
###18:21 | insgesamt 208 corona-neuinfektionen am sonntag
###18:01 | tschechien verlängert notstand nun doch
###16:47 | landkreise görlitz und sächsische schweiz lockern ausgangsbeschränkungen
###16:11 | dgb fordert ausnahmeregeln für tschechische grenzpendler nach bayerischem vorbild
###15:55 | landratsamt korrigiert: alle grenzübergänge zu tschechien bleiben offen
###15:24 | nach einreisestopp: tschechische corona-kritiker fordern vergeltung
###14:22 | starker ausflugsverkehr richtung moritzburg - polizei vor ort
###14:14 | bundespolizei: ruhe vor dem sturm an grenzübergängen
###13:17 | linke-fraktionschef gebhardt kritisiert kretschmers oster-absage
###12:54 | autoindustrie fürchtet werksschließungen in zwickau und leipzig
###12:02 | impfstoff kommt ins museum - zum drosten-räuchermännchen
###11:52 | sachsens linke schlagen unterricht in kultureinrichtungen vor
###09:20 | reitzenhain: vor den grenzkontrollen stand schneeräumen an
###09:02 | 165 fahrzeuge bei demo auf b96 und b99
###08:46 | sachsens winzer setzen auf online-weinverkostungen
###08:31uhr | kretschmer hält osterurlaub in deutschland für nicht realistisch
###08:18 | eisenbahn: grenzschließung zu tschechien wirkt sich auf binnenverkehr aus
###08:00 | kretschmer verteidigt grenzkontrollen zu tschechien
###07:40 | corona-verstöße bei gedenken in dresden
###07:30 | grenzkontrollen nach tschechien gestartet
