###21:39 | tickerende
###21:34 | 2g-stempel im praxistest in dresden und leipzig
###20:07 | omikron-variante jetzt auch in sachsen-anhalt, sachsen und thüringen dominant
###19:59 | kundgebung und menschenkette in chemnitz
###18:29 | sächsischer musikrat kritisiert corona-stufenplan
###17:09 | mehrheit der ausgeflogenen corona-patienten aus kliniken entlassen
###16:39 | neue quarantäneregeln für sachsen ab montag
###15:40 | dresdner forscher wollen pcr-schnelltest für covid-19 entwickeln
###15:12 | impfaktionen des landratsamtes in pirna und stolpen
###13:08 | kvs distanziert sich von impfkritischem text in freiberger praxen
###12:55 | temporäres impfzentrum in grimma schließt heute - kommende woche neues konzept
###12:40 | kultureinrichtungen in marienberg öffnen morgen wieder
###11:48 | dresdner museen laden zu kostenlosem schnupperbesuch ein
###11:14 | wendsche ruft stadtgesellschaft zum zusammenhalt auf
###10:23 | kleines impfzentrum nimmt in ebersbach-neugersdorf betrieb auf
###09:56 | landesjugendspiele im wintersport starten ohne zuschauer
###08:00 | chemnitzer initiative will heute der toten der corona-pandemie gedenken
###07:46 | 10.000. schutzimpfung bei vw sachsen
###06:51 | impfpflicht ist thema im sozialausschuss sächsische schweiz-osterzgebirge
###06:33 | bislang nur wenige bußgelder wegen corona-verstößen verhängt - leipzig an der spitze
###06:11 | corona-schutzimpfung für kinder im dresdner rathaus
###05:50 | in der corona-pandemie gekaufte reptilien bereiten tierheimen sorgen
###05:17 | aschenbrödel empfängt wieder gäste in moritzburg
