###21:35 | tickerende
###20:54 | simulation: so viele menschen würden ohne corona-beschränkungen sterben
###20:45 | profisport ohne zuschauer - geschlossene fitnessstudios
###20:34 | einzelhandel: nur grundversorgung für alle, sonst 2g
###20:20 | 3g-regelung am arbeitsplatz
###20:12 | ausgangsbeschränkung für ungeimpfte in hotspots
###19:49 | schulen und kitas bleiben offen
###19:35 | günther: kein allgemeiner lockdown
###19:30 | keine weihnachtsmärkte in sachsen
###18:48 | limbach-oberfrohna vergibt impftermine gegen das coronavirus
###18:30 | striezelmarkt-händler - zwischen hoffen und bangen
###17:16 | sachsen erlaubt ausnahmen beim arbeitszeitgesetz
###16:50 | handwerkstag erwartet klare antworten bei möglichem lockdown
###15:52 | annaberg sagt weihnachtsmarkt ab - zwickau verschiebt eröffnung
###15:27 | corona-fall beim fsv zwickau
###15:19 | linke fordern harten lockdown für sachsen
###15:07 | brauchen wir einen neuen lockdown. was sagen sie.
###14:34 | 25 soldaten unterstützen landkreis görlitz in corona-pandemie
###14:18 | dresdner gesundheitsamt kann kontakte nicht mehr nachverfolgen
###14:05 | keine weihnachtsmärkte im landkreis leipzig
###13:25 | dresdner frauenkirche sagt alle konzerte ab
###12:44 | kapazität der mobilen impfteams wird verdreifacht
###12:05 | mobiles impfteam in wermsdorf
###11:01 | bundesrat stimmt für neues infektionsschutzgesetz
###10:42 | kommunen kritisieren landesregierung
###10:08 | keine aktuellen corona-zahlen aus sachsen
###09:13 | chemnitz stoppt aufbau des weihnachtsmarktes
###08:40 | ab montag weitere einschränkungen in sachsen
###07:49 | absage von weihnachtsmarkt und pyramidenanschieben
###07:22 | schulschließungen in sachsen
###06:17 | kabinett beschließt weitere einschränkungen
###05:39 | länder für impfpflicht bei bestimmten gruppen
###05:21 | landkreis leipzig sucht freiwillige helfer im pflegedienst
###05:04 | wirtschaftshilfen verlängert
