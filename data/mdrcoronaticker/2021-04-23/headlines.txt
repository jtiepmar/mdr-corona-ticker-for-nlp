###20:38 | tickerende
###19:58 | auswirkungen der bundesnotbremse auf schulen und kitas im überblick
###19:52 | verwaltungsgericht chemnitz bestätigt demo-verbot
###19:25 | zahl der sächsischen corona-toten übersteigt 9.000er-marke
###19:10 | gedenken im kleinen kreis statt elbe day in torgau
###18:50 | notbremse sorgt für verwirrung in glauchauer baumarkt
###18:00 | mdrfragt mit umfrage zur mediennutzung
###17:21 | mittweida will mit gutscheinaktion händlern helfen
###16:52 | zwickauer landrat kritisiert schulschließungen
###16:36 | regeln für bundesnotbremse ab sonnabend in kraft
###15:49 | verkehrsbetriebe verlieren durch corona 30 prozent fahrgäste
###15:18 | ansturm auf impftermine
###15:07 | verbände fordern maßnahmen gegen welle an nichtschwimmern
###14:16 | sachsen zahlt 647 millionen euro wirtschaftshilfen aus
###13:44 | gemeinde wermsdorf klagt gegen bundesnotbremse
###12:21 | senioren in corona-pandemie häufiger ziel von betrügern
###11:52 | tag der erneuerbaren energien in oederan virtuell
###11:35 | ostsächsische polizei bereitet sich auf corona- ringspaziergang vor
###11:10 | zahl der übernachtungen in sachsen eingebrochen
###10:53 | ab montag bleiben schulen und kitas in nur vier landkreisen offen
###10:37 | kassenärzte-chef heckemann für aufhebung der impfpriorisierung
###10:03 | neuer test-bus im landkreis zwickau
###09:36 | wochen-inzidenz in sachsen steigt wieder leicht
###09:04 | freistaat schließt grundschule in burkhardtsdorf
###07:54 | museen und schlösser in sachsen wieder geschlossen
###06:50 | trotz corona-impfung: flickenteppich bei besuchen in pflegeheimen
###06:08 | schülerinnen und schüler starten in abiturprüfungen
###05:56 | dynamo trainiert wieder
###05:41 | bundes-notbremse greift ab samstag
###05:05 | nachgeholte trauerstunde
