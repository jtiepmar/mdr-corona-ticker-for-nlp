###21:10 | tickerende
###20:14 | genesenenstatus künftig nur noch digital
###19:50 | kretschmer: teile der bevölkerung für argumente unerreichbar
###19:05 | friedliche gegenproteste in leipzig
###18:06 | kammern und handelsverband fordern deutlich mehr lockerungen
###17:44 | 990 covid-19-tote in dresden 2021
###17:05 | rb-leipzig-spieler nicht mehr in quarantäne
###16:52 | aktion für anstand und mitmenschlichkeit in pirna
###16:17 | theater plauen-zwickau bereitet spielbetrieb vor
###16:01 | grüne wollen perspektiven für jugend und kultur, fdp für gastronomie
###15:34 | dresden will projekte für zusammenhalt in der pandemie fördern
###15:15 | fenster in glauchau funkeln länger
###14:29 | forschung für genauere daten zur infektionslage
###14:04 | ärzte-erklärung zu impfstoff-falschmeldungen
###13:15 | impfungen für kinder im krankenhaus in bautzen
###12:40 | protest gegen corona-spaziergänger in leipzig
###12:28 | stillstand oder weitermachen. - eine fußballkolumne
###11:49 | infotag an hochschulen und berufsakademie
###11:12 | viele schnelltests erkennen omikronvariante
###10:48 | impfaktion in gesundheitsämtern delitzsch und torgau
###10:22 | wenig flugverkehr über deutschland
###09:54 | gelungene impfaktion im theater von bautzen
###09:25 | rückblick auf intensivbettenprognose
###08:51 | corona-impfung im dresdner rathaus
###08:30 | impfungen im alten arbeitsamt in glauchau
###07:57 | wer soll den pflegebonus erhalten.
###07:13 | uni leipzig beginnt mit präsenzlehre
###06:46 | inzidenzwert ist gestiegen
###06:10 | proteste am wochenende
###05:41 | schnelltests werden auf wirksamkeit überprüft
###05:12 | umfrage zu geplanten corona-lockerungen
###05:03 | dampfeisenbahnen in pandemie mit weniger fahrgästen
