###20:09 | tickerende
###19:06 | freiberg wehrt sich gegen corona-proteste
###18:29 | impfungen in sachsen auch zu weihnachten und silvester
###17:30 | sachsen passt corona-notfallverordnung an
###17:13 | sächsische impfkommission rechnet mit mehr infektionen
###17:02 | polizei beschlagnahmt gefälschte impfausweise in leipzig
###16:40 | was darf der weihnachtsmann.
###16:10 | studie der uni leipzig zum umgang mit der pandemie
###15:32 | verstößt eine allgemeine impfpflicht gegen das grundgesetz.
###15:16 | ovg-urteil: weiter 2g-regel in gastronomiebetrieben sachsens
###14:59 | elblandkliniken wollen impfquote erhöhen
###14:34 | bundesregierung bestellt neuen impfstoff
###14:19 | etwa ein viertel weniger übernachtungen in dresden
###13:55 | niners-partie in chemnitz abgesagt
###13:31 | frei verwendbare betriebskostenpauschale für unternehmen in not
###13:17 | linke: nachsicht der politik hat spezielle protestkultur ermöglicht
###12:43 | randalierer und maskenverweigerer angeklagt
###11:57 | narren hadern mit faschingsumzug
###11:01 | ethikrat für ausweitung der impfpflicht
###10:25 | untersuchungen zur verbreitung von virusvariante
###09:44 | impfaktion in mittweida
###09:26 | sachsen will kontakte weiter einschränken
###08:45 | impfbilanz im landkreis görlitz
###08:29 | freiberg will mit imagekampagne ruf retten
###07:51 | corona-wocheninzidenz in sachsen bei 580
###07:22 | virtueller neujahrslauf
###06:53 | impfen für spontane in machern, mügeln und rackwitz
###06:23 | rücktritt nach impfvergleich mit völkermord
###05:45 | drk startet terminvergabe für kinderimpfung
###05:22 | erneute beratungen im kabinett
###05:09 | bundesliga wieder mit geisterspielen
