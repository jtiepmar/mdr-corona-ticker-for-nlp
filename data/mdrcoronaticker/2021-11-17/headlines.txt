###21:30 | tickerende
###20:57 | hausärzte-chef warnt vor überlasteten praxen und personalausfällen
###20:30 | team sport sachsen will boosterimpfungen unterstützen
###20:15 | rki-chef wieler begründet hohe infektionslage trotz impfungen
###19:45 | leipziger chefarzt warnt vor problematischer lage für notfallpatienten
###19:20 | klinikkoordinator albrecht fordert komplett-lockdown für 14 tage
###18:45 | rki-chef wieler und meyer-hermann kritisieren zu langsame entscheidungen
###18:30 | großer andrang bei impfaktion in pirna
###18:05 | kretschmer und köpping diskutieren mit wissenschaftlern
###17:55 | sächsischer fußballverband sagt spiele ab
###17:06 | früherer thomaskirch-pfarrer gegen corona-drohszenarien
###15:48 | 2g - ihk dresden erwartet kraftakt für einzelhandel
###15:48 | soldaten aus oberpfalz helfen in bautzner gesundheitsamt
###15:20 | französische filmtage in leipzig mit strengen corona-regeln
###15:06 | merkel fordert schwellenwert für klinikbelastung
###14:45 | wie sinnvoll ist eine impfpflicht für pflegekräfte.
###14:23 | ab freitag überlastungsstufe in sachsen
###14:20 | drittliga-spiel zwischen zwickau und magdeburg abgesagt
###14:05 | corona-ausbruch in jva görlitz
###13:47 | 3g-regelung bei leipziger reisemesse
###13:18 | hausärzteverband: auffrischungsimpfung erst für über 70-jährige
###11:36 | einblicke in die covid-intensivstation in der uniklinik dresden
###11:22 | einige menschen offenbar super-resistent gegen sars-co v-2
###10:41 | krankenhäuser brauchen blutkonserven
###10:13 | zunehmend verbale und körperliche gewalt bei corona-kontrollen
###09:48 | thw soll beim impfen und testen unterstützen
###09:06 | landkreis meißen bundesweit mit höchster inzidenz
###08:14 | chemnitz will impfzentrum wieder öffnen
###07:32 | einzelhandel leidet unter corona-welle
###07:07 | corona-welle erneut thema bei fakt ist.
