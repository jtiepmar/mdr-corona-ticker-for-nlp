###20:30 | tickerende
###20:06 | exakt-reportage corona impfung - wie radikal sind impfgegner im osten.
###19:25 | boostern mit hindernissen - sachsens ärztevertreter für priorisierung
###18:31 | unbekannter attackiert impfteam in dresden
###18:00 | corona-hilfen für weihnachtsmarkthändler in sachsen
###17:42 | razzien nach betrug mit corona-hilfen in dresden und görlitz
###16:58 | abgesagter winterurlaub: rechte von verbrauchern
###16:33 | scholz kündigt härtere pandemie-bekämpfung an
###15:44 | verlegung von 20 corona-intensivpatienten aus sachsen beantragt
###15:25 | hohe impfnachfrage in sachsen - kabinett prüft weitere erhöhung der kapazitäten
###15:14 | vogtland-bürgermeister kritisieren corona maßnahmen
###15:10 | landratsamt zwickau: kaum grobe verstöße gegen corona-auflagen
###15:00 | köpping bedauert anfängliche ablehnung einer impfpflicht
###14:45 | auch 2021 keine weihnachtsvesper vor dresdner frauenkirche
###14:28 | unternehmen rechnen mit holprigen start bei 3g-pflicht am arbeitsplatz
###14:22 | impfambulanz in aue-bad schlema eröffnet
###13:10 | leipzig: ohne kinderpunsch und weihnachtsmarkt - sammlertassen gibt es trotzdem
###12:50 | linke und grüne in sachsen wollen schulpflicht aussetzen
###11:23 | paul-ehrlich-institut: nicht alle schnelltests liefern zuverlässige ergebnisse
###11:11 | noch keine entscheidung über verlängerte weihnachtsferien in sachsen
###10:39 | dresdner reisebüros wollen gegen corona-notverordnung klagen
###09:09 | 350 rathaus-mitarbeitende in dresden fürs gesundheitsamt geschult
###08:35 | spahn von impfunwilligen frustriert
###07:55 | albrecht: impfpflicht kann erst in nächster welle helfen
###07:15 | pegauer karnevaliste wollen nicht für corona im rathaus verantwortlich sein
###07:04 | neue impfangebote in leipzig und markkleeberg
###06:46 | 50 polizisten im raum dresden zu corona-kontrollen unterwegs
###06:25 | polizei verhindert demo in zwönitz
###05:35 | präsident von erzgebirge aue verlangt fußball-lockdown
###05:06 | hohe corona-fallzahlen: dresden stockt bei gesundheitsamt auf
