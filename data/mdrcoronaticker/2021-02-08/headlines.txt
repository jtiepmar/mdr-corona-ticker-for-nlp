###20:24 | tickerende
###19:37 | keine nachweise von mutation in leipzig seit januar
###18:50 | insgesamt 232 neuinfektionen am montag in sachsen
###18:32 | kretschmer dämpft hoffnung auf baldige lockerungen
###18:10 | impfstofflieferung wetterbedingt verschoben
###17:55 | weniger geburten und viele sterbefälle im januar in dresden
###17:13 | britische coronavirus-variante breitet sich im erzgebirge aus
###16:28 | hunderte polizeikräfte gegen corona geimpft
###16:21 | corona-maßnahmen helfen auch gegen grippewelle
###16:03 | bürgerdialog der landesregierung zu corona
###15:38 | schneechaos sorgt für probleme bei impfterminen
###15:20 | ärzte in pflegeeinrichtungen benötigen negativen schnelltest
###14:06 | zahl der ostsächsischen handwerksbetriebe stabil
###13:42 | ermittlungen gegen görlitzer kaufhaus-chef winfried stöcker
###13:30 | mehrere kitas in treuen bleiben zu
###13:05 | piwarz: weiterführende schulen sollen im märz öffnen
###12:29 | wundertüte aus dem radebeuler lügenmuseum
###12:13 | klinikmitarbeiter bekommen erneut corona-prämie
###11:31 | landkreis leipzig gibt armen familien geld für laptops
###11:04 | kassenärztevereinigung: hausärzte eher impfen
###10:41 | spiel zwischen rb leipzig und fc liverpool in der kritik
###10:24 | katholische akademie lädt zum gespräch ein
###09:40 | uniklinikum dresden übergibt dokumente ans stadtarchiv
###08:41 | schule im lockdown heute thema in nikolaikirche
###08:06 | vogtlandkreis in alarmbereitschaft
###07:25 | virusmutation in bad düben nachgewiesen
###06:52 | altmaier und spahn für längeren lockdown
###06:21 | händlerproteste gegen zwangsschließungen
###05:56 | bürgertalk zu lockdown-auswegen
###05:19 | manege frei für die corona-nummern
###05:04 | neues testzentrum in limbach-oberfrohna
