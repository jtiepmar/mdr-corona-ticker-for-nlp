###19:21 | tickerende
###19:10 | mehrheit für öffnung der hotels und restaurants zu ostern
###18:29 | inzidenz in sachsen steigt weiter
###17:55 | europäische städte planen schweigeminute
###17:31 | landkreis mittelsachsen muss lockerungen zurücknehmen
###16:30 | corona-testpflicht für unternehmer ab kommender woche
###15:46 | elternproteste im erzgebirge
###15:30 | kassenärzte-chef für mehr impfungen in arztpraxen
###14:53 | ministerpräsident kretschmer gegen weitere lockerungen
###14:44 | pandemie als preis für ausbeutung der natur
###14:29 | lockdown bis april im gespräch
###14:03 | uniklinikbesuch nur noch mit negativem test
###13:19 | deutsches kinderhilfswerk kritisiert deutsche corona-politik
###12:36 | polens grenzpendler fordern finanzielle hilfe von deutschland
###12:15 | sachsens lehrkräfte fordern mehr rückhalt und klare regeln
###11:49 | oberwiesenthal setzt auf tourismus trotz corona
###11:12 | biontech-gründer: lockdown-maßnahmen enden im herbst
###10:58 | dialysepatienten und nierentransplantierte vollständig geimpft
###10:35 | keine züge mehr zwischen sachsen und polen
###10:01 | jung: akzeptanz von corona-maßnahmen schwindet
###09:35 | zu viele teilnehmer - demo in aue aufgelöst
###09:04 | inzidenz bundes- und landesweit weiter gestiegen
###08:10 | jetzt strengere regeln für einreise aus polen
