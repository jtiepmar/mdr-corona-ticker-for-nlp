###18:55 | tickerende
###18:49 | inzidenz: alle landkreise und kreisfreien städte unter 35er-marke
###17:52 | dresdner elbwiesenreinigung nachgeholt
###17:00 | nur 50 prozent der corona-todesfälle lassen sich derzeit verhindern
###16:12 | museen in zittau ohne test und anmeldung besuchen
###15:22 | start der jugendweihen in chemnitz
###14:30 | betriebsärzte bekommen serum von johnson &amp; johnson
###13:52 | hohe impfquote schützt ungeimpfte jugendliche
###13:30 | sachsen weiter mit niedrigster erstimpfquote
###12:33 | leipziger schauspielhaus mit digitaler uraufführung
###11:39 | kekulés corona-kompass-spezial: neue nebenwirkungen bei m rna-impfstoffen
###11:12 | städtetag fordert weiterbetrieb der impfzentren
###10:44 | tanzen wie vor corona: test in leipziger club distillery
###09:01 | intensiv-pflegekräfte weiter am limit
###08:32 | zahl der neuinfektionen sinkt weiter
###08:10 | unter corona-bedingungen ein filmset reinigen
###08:00 | gastronomie kämpft mit personalmangel
