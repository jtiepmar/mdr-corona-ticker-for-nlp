###18:45 | tickerende
###18:30 | helferpool gegen akute personalengpässe in nordsachsen
###17:53 | inzidenz in dorfchemnitz bei 2.836,4
###16:37 | uniklinik dresden richtet weitere normalstation für covid-kranke ein
###16:10 | sieben-tage-inzidenz bei ungeimpften auf fast 1.000 gestiegen
###16:03 | chemnitzer fc verschiebt mitgliederversammlung
###16:00 | innenminister wöller in corona-quarantäne
###15:19 | sachsen will impfkapazitäten mobiler teams verdoppeln
###13:10 | mobile impfteams am limit
###12:58 | fünf schulen wegen corona-fällen vorübergehend geschlossen
###11:47 | weißwasser sagt wegen 2g weihnachtsmarkt ab
###11:37 | chemnitz kontrolliert verstärkt 2g-regeln
###09:07 | sächsischer bergsteigerchor bläst jahreskonzerte ab
###07:41 | sächsisches kabinett berät maßnahmen gegen corona-ausbreitung
###07:15 | kita in aue zwei wochen dicht
###06:50 | appell von landrat geisler: bürger sollen kontakte beschränken
###06:29 | bundeswehr kündigt mehr unterstützung an
###06:02 | eingeschränkter kundenservice bei stadtwerken aue-schlema
###05:32 | unmut bei profi-sportvereinen
###05:00 | zwickauer tafel wegen corona geschlossen
