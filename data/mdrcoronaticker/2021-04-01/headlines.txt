###21:44 | tickerende
###20:59 | bald impfen ohne pieks.
###18:10 | 1.019 betten auf normalstation mit corona-patienten belegt
###17:35 | neue regeln - der mdr sachsen-überblick
###17:29 | schnelltest-zentren in nordsachsen über feiertage geschlossen
###16:59 | uniklinik dresden impft schwer erkrankte patienten
###16:15 | schnelltest-drive-in in chemnitz eröffnet
###15:54 | post-covid-ambulanz in schkeuditz verlängert sprechzeiten
###15:40 | kita- öffnungen in chemnitz und plauen, in leipzig personalmangel
###14:50 | bürgermeister von moritzburg möchte keinen massenanlauf zu ostern
###14:30 | testmöglichkeiten über ostern in der oberlausitz
###14:12 | unterstützung für bautzener gewerbetreibende
###13:15 | dehoga: restaurants bieten über ostern fertige menüs zum abholen
###13:00 | deutsche bahn weitet fernverkehr der verbindung prag - berlin aus
###12:43 | landesbischofs bilz: osterbotschaft gibt hoffnung in schwerer zeit
###12:30 | bundespräsident steinmeier lässt sich mit astrazeneca impfen
###11:25 | wissenschaft bewertet astrazeneca-nebenwirkungen
###11:00 | leipzigerinnen und leipziger müssen vorerst wieder mit einschränkungen leben
###10:43 | arbeitsgruppe impfunterstützung der stadtverwaltung plauen hat arbeit eingestellt
###10:27 | augustusburg startet modellprojekt zum lockdown-ausstieg
###09:29 | ikarus-bustreffen in chemnitz abgesagt
###09:00 | landesbühnen sachsen sagen alle vorstellung ab - märchen gibt es aber digital
###08:32 | testzentrum in neuem rathaus leipzig an osterfeiertag und karfreitag zu
###08:20 | zuschendorfer kamelien können doch besichtigt werden
###07:28 | mehr parkplätze am impfzentrum belgern
###06:46 | drive-in-schnelltests auf chemnitzer stadionparkplatz möglich
###06:15 | campingplätze hoffen auf saisonstart im mai - andernfalls drohen pleiten
###06:00 | dresden schränkt öffentliches leben wieder ein
###05:50 | magier verzaubert seine publikum - von dresden aus online
###05:34 | vorm friseurbesuch ab heute mindestens selbsttest nötig
###05:20 | fast 300 infektionen in kitas und schulen im landkreis bautzen
###05:03 | chemnitzer ostermarsch wird kundgebung
