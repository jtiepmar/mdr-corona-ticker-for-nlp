###21:15 | tickerende
###21:02 | silvestermusik vor leeren rängen
###20:39 | leipziger corona-protest-gruppe als extremistisch eingestuft
###20:25 | mdr-umfrage zur weihnachtsstimmung
###20: 14 | weihnachtsoratorium in leipziger nikolaikirche nur im netz
###19:53 | silvesterfeuerwerk nur hinterm gartenzaun
###19:41 | mehr infektionen mit keimen in der corona-pandemie
###19:01 | weniger neuinfektionen gemeldet
###18:44 | schule: keine früheren weihnachtsferien, keine schärferen regeln
###18:37 | regelstudienzeit wird erneut verlängert
###18:19 | sechs omikron-verdachtsfälle in sachsen
###17:56 | sachsen verschärft kontaktbeschränkungen
###17:28 | freie impftermine in freiberg
###17:00 | einzelheiten zur neuen corona-verordnung
###16:48 | sachsen bleibt schlusslicht beim impfen
###16:23 | keine ausgangssperre mehr im leipziger land
###15:58 | oberverwaltungsgericht weist weitere corona-klagen ab
###15:11 | bürgerinitiative bautzen gemeinsam kritisiert corona-protest
###14:43 | begrenzte impfpflicht kommt
###14:25 | impfen im dresdner fußball-stadion
###13:23 | forscher: coronavirus befällt menschliches stress-system
###13:08 | semperopern-ball fällt erneut aus
###11:55 | freie impftermine in plauen
###11:49 | olbernhau schließt museen und kultureinrichtungen
###11:30 | kultusminister halten an offenen schulen fest
###11:12 | mehrheit der deutschen wollen weihnachten kontakte reduzieren
###09:15 | bundestag stimmt über infektionsschutzgesetz ab
###07:15 | bund will keine weiteren einschränkungen über weihnachten
###06:45 | laden auf zeit als ersatz für weihnachtsmarktbuden
###06:15 | taucha dankt der bundeswehr mit banner
###05:56 | dresden: impfung mit musik
###05:35uhr | rechtsexperte: impfpflicht ist kein impfzwang
###05:22 | hainichen: moderna-impfungen in historischem ambiente
###05:06 | trübt die corona-krise sogar das chemnitzer kulturhauptstadtjahr.
