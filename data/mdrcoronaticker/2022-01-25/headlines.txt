###21:58 | tickerende
###20:50 | dynamo-geschäftsführer wehlend kritisiert geringe zuschauer-quote
###20:29 | landestourismusverband begrüßt änderungen der corona-notverordnung
###20:13 | kekulé sieht keinen grund für corona-kursänderung
###20:02 | ihk chemnitz begrüßt sachsen plus
###19:46 | herzentzündung als seltene nebenwirkung der corona-impfung
###19:28 | long-covid – die lange suche nach hilfe
###19:02 | heftige debatte nach rede von vize-landrat aus bautzen
###18:49 | weiterer corona-fall bei leipziger eishockeyteam
###18:30 | impfpflicht: verbände warnen vor zusammenbruch der pflege
###18:16 | labormediziner fordert pcr-pooltests und labor-antigentests
###18:03 | impfappell bei online-bürgersprechstunde
###17:28 | keine lockerungen für überregionale großveranstaltungen
###16:48 | köpping und experten beantworten bürgerfragen live
###16:33 | bilanz: 53.000 menschen demonstrieren gegen corona-regeln
###16:17 | köpping kritisiert falschinformationen zur impfung
###16:00 | kein abbau der schuldenlast im vergangenen jahr
###15:44 | biontech und pfizer starten klinische studie mit omikron-impfstoff
###15:28 | zahl der erwerbstätigen stabil
###14:55 | grund für milderen omikron-verlauf gefunden
###14:33 | neue finanzhilfen für freiberufler und kleine unternehmen
###14:14 | corona-notfallverordnung soll unverändert bleiben
###13:14 | trotz corona mehr ausbildungsverträge im handwerk
###12:54 | winterurlaub mit 2g-plus
###12:30 | landkreis meißen will impfpflicht umsetzen
###11:59 | corona-test auf dem mikrochip
###11:08 | impfservice für menschen mit behinderung
###10:22 | das kostet die behandlung von corona-patienten
###08:43 | kretschmer kritisiert verkürzten genesenenstatus
###07:58 | lernen daheim am bildschirm
###07:12 | impfpflicht im gesundheitsbereich in frage gestellt
###06:42 | verlage setzen auf leipziger buchmesse
###06:09 | wocheninzidenz in sachsen bei 377
###05:41 | proteste gegen corona-maßnahmen
###05:20 | regierung stellt heute eckpunkte neuer verordnung vor
###05:08 | tourismusbranche will nicht wieder in lockdown
