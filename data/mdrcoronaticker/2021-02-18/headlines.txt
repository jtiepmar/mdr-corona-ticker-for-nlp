###21:16 | tickerende
###21:10 | patientenbesuche im krankenhaus großschweidnitz wieder möglich
###20:35 | teilschließung in bautzener kita wegen corona-fall
###19:50 | landkreis mittelsachsen setzt auf rollendes impfzentrum
###19:27 | kita-personal soll regelmäßig auf corona gestetet werden
###18:59 | 7-tage-inzidenz: leipzig am niedrigsten, nordsachsen wieder über 100
###18:36 | verwaltungsgericht: frühstücksräume bleiben für hotelgäste tabu
###17:29 | 98 nachweise von corona-mutationen in sachsen
###17:02 | wirtschaftsförderung fordert schnelles ende für grenzschließung
###16:36 | seehofer verteidigt kontrollen an grenze zu tschechien
###15:36 | 400 friseure und kosmetiker protestieren auf dresdner altmarkt
###14:53 | vogtländer serverfirma profitiert von corona
###14:27 | chemnitz will unternehmer entlasten
###13:45 | wirrwarr um corona-zahlen in leipzig
###13:22 | ifo-institut sieht schnelle tilgung der corona-schulden kritisch
###13:10 | polizei beendet party in zwickau
###13:02 | bundespräsident ruft menschen zu geduld auf
###12:56 | mehrere oberlausitzer gemeinden ohne corona-fälle
###12:41 | corona-impfungen in 94 prozent aller alten- und pflegeheime
###12:23 | zwei weitere fälle von corona-mutation im erzgebirge
###12:11 | videoservice der kreissparkasse bautzen
###11:52 | servicestelle für impfungen in hoyerswerda kommt gut an
###11:38 | noch über 2.500 freie impftermine in dieser woche
###11:24 | wieder ausgangsbeschränkungen in sächsischer schweiz
###11:13 | projekt der tu dresden gibt überblick über weltweite corona-forschung
###10:57 | zahl der übernachtungen in sachsen eingebrochen
###10:33 | landratsämter bautzen und görlitz stellen bescheinigungen für grenzpendler aus
###10:21 | deutsche industrie erholt sich weiter
###09:55 | kretschmer bleibt dabei: kein osterurlaub in diesem jahr
###09:23 | sächsische apothekerinnen und apotheker unterstützen kostenlose schnelltests
###09:06 | verdi fordert wegen corona humanere touren-planung für lkw-fahrer
###08:19 | grenzverkehr läuft ohne probleme
###07:26 | astra-zeneca-impfstoff wird häufig abgelehnt
###07:11 | bautzen setzt weiter auf hilfe der bundeswehr
###06:58 | kommunen warnen vor überzogenen erwartungen an schnelltests
###06:36 | rechtsextreme musikszene trotz corona aktiv
###06:14 | handwerkstag fordert gleichbehandlung von friseur und kosmetik
###06:01 | großer corona-ausbruch in leipziger pflegeheim im januar
###05:41 | bundesinnenminister besucht grenzübergang breitenau
###05:19 | grenzübertritt: nach arbeitgeber muss auch das amt bestätigen
###05:00 | jeder dritte hat in der corona-zeit zugenommen
