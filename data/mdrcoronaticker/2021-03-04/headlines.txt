###21:26 | tickerende
###21:22 | chemnitz will mit corona-apps zurück zur normalität
###20:41 | mobilität in sachsen höher als vor der corona-krise
###20:26 | leipziger bündnis stellt sich querdenkern in den weg
###19:36 | gericht kippt ausgangssperre und 15-kilometer-radius
###19:31 | zahl der privatinsolvenzen in sachsen geht zurück
###18:31 | sachsens inzidenzwert kurz vor 100er-marke
###18:08 | dresden will impftaxis für über 80-jährige anbieten
###17:01 | spd: vorsichtige schulöffnungen sind richtig
###16:51 | afd lehnt corona-kurs von kretschmer ab
###16:30 | sachsen will wieder mehr freiheit ermöglichen
###16:17 | annaberger grundschulen haben jetzt luftfiltergeräte
###15:06 | nordsachsen: noch keine entscheidung über schul- und kitaschließungen
###14:49 | handwerkstag-präsident: lockerungen sind überfälliger schritt
###14:18 | landestourismusverband sachsen beklagt fehlende öffnungsperspektive
###13:54 | ihk dresden begrüßt ausstieg aus dem lockdown
###13:40 | rb leipzig spielt in champions league auch rückspiel in budapest
###12:44 | bürgerallianz wirbt für kauf einheimischer produkte
###12:18 | reisegruppe fliegt mit gefälschten coronatests auf
###11:52 | arbeitsrechtler erklärt schnelltest-anordnung
###11:30 | mehr beschäftigte in kurzarbeit
###10:59 | helios-konzern verhandelt nicht über corona-prämie
###10:37 | chemnitzer impfzentrum soll größer werden
###09:48 | lufthansa mit rekordverlust
###09:39 | impftelefon von plauen ist gefragt
###09:01 | fdp sieht sachsens regierung in der pflicht
###08:33 | erfolge bei behandlung von covid-19-patienten
###08:10 | weiter keine corona-fälle bei massentest in radeberg
###07:47 | leipzigs bibliotheken öffnen eingeschränkt
###07:15 | dresdner stadtrat berät über maßnahmen in schulen und kitas
###06:53 | hausärzte fordern weniger bürokratie beim impfen
###06:28 | mehr als 1.000 corona-patienten in oberlausitzkliniken behandelt
###06:04 | beratungen zur neuen corona-schutzverordnung
###05:34 | neue überbrückungshilfe auch für große unternehmen
###05:12 | kein schwimmunterricht in corona-zeiten
###05:05 | zukunft der tourismusbranche
