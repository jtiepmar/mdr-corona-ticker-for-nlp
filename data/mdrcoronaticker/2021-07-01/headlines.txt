###20:30 | tickerende
###19:14 | sieben landkreise ohne corona-neuinfektionen
###18:56 | städtetag ruft zu impfungen auf
###17:31 | stiko ändert impfempfehlung
###16:52 | sachsen hat noch freie ferienunterkünfte
###16:28 | corona-verstöße führen zu anstieg bei bußgeldverfahren
###16:01 | leipzig startet unterstützungspaket für die innenstadt
###15:35 | fehlende fachkräfte bremsen neustart im gastgewerbe
###15:10 | app impf-finder spürt impftermine bei niedergelassenen ärzten auf
###14:47 | impfaktion im ergebirgsstadion
###13:49 | immer mehr patienten wegen covid-folgen arbeitsunfähig
###13:02 | altmaier: aufschwung für ausbau der halbleiterfertigung nutzen
###12:46 | keine impfpass-selfies ins netz stellen
###12:22 | sachsens kinobetreiber legen wieder los
###11:33 | kostenlos parken in großenhain
###11:25 | experteninterview zu umgangsrechten in der pandemie
###10:59 | neuer chefarzt im krankenhaus von zittau
###10:27 | gelockerte corona-regeln in urlaubsländern
###09:52 | kinosaalversteigerung für einen guten zweck
###09:17 | dresdens aussichtstürme machen auf
###08:48 | bei anruf krankenschein.
###08:19 | bonusaktion beim innenstadthandel in werdau
###07:53 | testzentren in leipzig und delitzsch schließen
###07:20 | wocheninzidenz in sachsen bleibt niedrig
###06:36 | eu gibt startschuss für digitalen impfpass
###06:09 | wettbewerb markneukirchen für horn und tuba ausgeschrieben
###05:41 | corona beschert leipziger uniklinikum millionenverlust
###05:28 | corona und die folgen - sieben schicksale aus mitteldeutschland
###05:09 | neue corona-schutzverordnung: was sich jetzt ändert
