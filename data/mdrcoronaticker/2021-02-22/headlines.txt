###21:19 | tickerende
###20:26 | hohe nachfrage bei click &amp; collect
###19:46 | impfstofflieferungen nehmen deutlich zu
###19:25 | staatshilfen - das thema bei fakt ist.. am montag
###18:37 | die aktuellen corona-zahlen für sachsen
###18:02 | polizei löst illegalen ausschank in schwarzenberg auf
###17:42 | bühnenverein will einheitliche rückkehr aus kulturlockdown
###17:23 | keine informationen über impfungen von lehrern und polizisten
###16:56 | semperoper dresden streamt wieder
###16:09 | deutsch-tschechischer zukunftsfonds leistet hilfszahlungen
###15:58 | zahlreiche polizeieinsätze in der oberlausitz am wochenende
###15:36 | drk-kreisverband bautzen bittet um pünktlichkeit im impfzentrum
###15:20 | am wochenende: 74 einsätze wegen corona-verstößen in dresden
###14:48 | freiberger händler protestieren gegen corona-beschränkungen
###14:34 | kreissportbünde der oberlausitz machen vorschläge zum restart des vereinssports
###14:02 | zwischenbilanz zu schnelltests des landratsamts bautzen
###13:47 | handwerkskammer dresden: freistaat soll tests für friseure bezahlen
###12:58 | bundesregierung will digitaler bildung einen schub verleihen
###12:55 | corona wirft tourismus in görlitz knapp zehn jahre zurück
###12:39 | polen denkt über negativtest bei einreise nach
###12:28 | tschechien riegelt bäderregion um karlsbad ab
###10:55 | bei click &amp; collect muss bargeld akzeptiert werden
###09:30 | zeitung: ministerium sucht humanitäre lösung für getrennte familie
###09:19 | reiseveranstalter alltours kündigt urlaub nur mit impfung an
###08:45 | corona-krise wirkt sich auf handel unterschiedlich aus
###07:00 | freiberger händler protestieren gegen corona-beschränkungen
###06:45 | corona-krise vernichtet tausende mini-jobs
###06:30 | wegen mutationen: spahn gegen konkreten lockerungsplan
###06:18 | großer zuspruch: erste impftermine im bus
###05:58 | dresdner stadtverkehr wieder im zehn-minuten-takt
###05:50 | städtetag berät über neue konzepte für innenstädte nach befürchteten corona-pleiten
###05:19 | handwerkskammer verschickt gesellenbriefe und partypakete
###05:00 | infektionsrisiko im mehrpersonenbüro am größten
