###22:00 | ticker-ende
###21:45 | vorschau auf bund-länder-schalte
###21:30 | corona-impfung auch für bereits infizierte möglich
###20:55 | kretschmer gegen kompletten lockdown
###20:40 | ansturm auf sächsische impf-hotline
###20:25 | gesundheitsamt meißen: lockdown-verlängerung ja, verschärfung nein
###19:55 | talk-runde mit ministerpräsident und virologin
###19:00 | aktuelle corona-zahlen aus dem freistaat
###18:35 | covid-simulator schätzt infektionsgeschehen ein
###18:10 | hunderte verstöße gegen corona-schutzverordnung
###17:35 | quarantäne-ende für landwirtschaftsminister günther
###17:10 | freiberger händler fordern staatliche unterstützung
###16:50 | vorbereitung von räumen für quarantäneverweigerer
###16:35 | dynamo will nach negativ-testreihe wieder zusammen trainieren
###15:45 | impfstopp in sachsen erst ab montag
###14:40 | ministerpräsident: lockdown bis 14.februar denkbar
###14:15 | dulig: vorerst kein abholservice im sächsischen handel
###13:50 | mdr wissen stellt täglich lern-angebote zur verfügung
###12:57 | 600 registrierungen bei chemnitzer hilfsangebot
###12:42 | änderungen bei quarantäne-regelung
###12:04 | spahn ordnet suche nach virus-mutationen an
###11:12 | bund finanziert ffp2-masken für 34 millionen bürger
###10:56 | überbrückungshilfe für kreative: top oder flop.
###10:36 | corona-verstöße an deutsch-tschechischer grenze
###10:22 | jung fordert schärfere corona-maßnahmen
###10:05 | viele sachsen haben sich mit lockdown-maßnahmen arrangiert
###09:26 | epidemiologe hält ziel von zero covid für nicht machbar
###09:16 | durchwachsenes jahr 2020 für sachsens einzelhandel
###08:54 | temporärer impfstopp voraussichtlich ab donnerstag
###08:40 | petition fordert einheitliches konzept für homeschooling
###08:26 | petra köpping beantwortet fragen zur impfung
###08:13 | cvag verkehrt wieder normal
###07:59 | hausärzte arbeiten an der belastungsgrenze
###07:40 | sparkasse meißen zieht servicepersonal ab
###07:29 | kritik an schulstart und corona-test
###07:20 | wieder normalfahrplan in nordsachsen
###07:08 | zweite impfdosis im pflegeheim lichtentanne
###06:48 | virologe zur unterbringung von quarantäne-verweigerern
###06:22 | zugverkehr zwischen sachsen und polen wieder planmäßig
###05:55 | schulen in sachsen öffnen für abschlussklassen
###05:30 | corona-testpflicht für berufspendler ab heute
###05:20 | filmfest dresden wächstz - trotz krise
###05:07 | #gernelernen mit mdr wissen und der uni leipzig
