###19:00 | tickerende
###18:35 | unter vorbehalt: 1.058 neue coronainfektionen in sachsen
###18:15 | impfzentrum im vogtland nimmt vorzeitig arbeit auf
###16:50 | polizei will kontaktbeschränkung in sachsen nicht ohne anlass kontrollieren
###16:30 | deutscher hausärzteverband fordert lockerungen der corona-einschränkungen für geimpfte
###15:40 | nummer 586 oder 587: welcher striezelmarkt findet dieses jahr statt.
###14:15 | hohe impfbereitschaft bei personal von uni-kliniken
###12:00 | polizei kontrolliert corona-regeln in wintersportgebieten
###11:43 | kampagne gegen häusliche gewalt in lockdown-zeiten
###10:11 | städtetag fordert: impfkampagne muss zügig an fahrt aufnehmen
###10:02 | dickes besucherminus in sachsens zoos und tiergärten
###09:12 | wenige corona-fälle in sachsens gefängnissen
###08:07 | kretschmer räumt fehler bei krisenmanagement ein
###07:50 | dulig appelliert an bundeswirtschaftsminister altmaier
###07:35 | ehrenamtliche bei der tafel legen wieder los
###07:20 | dgb kritisiert vorgehen bei grenzpendler-tests
###07:00 | heilbäder halten sich mit mühe über wasser
