###21:00 | tickerende
###20:52 | impfung bei hausärzten erst mitte april
###20:40 | sachsen gibt weitere 60 millionen euro für impfkonzept frei
###20:32 | leipziger wissenschaftler entwickelt laborkoffer
###20:25 | viele kundinnen und kunden finden click &amp; meet umständlich
###20:19 | neue teststation in eilenburg
###20:00 | 799 gemeldete neuinfektionen am mittwoch
###18:39 | görlitzer landrat alarmiert wegen steigender inzidenz
###17:57 | handballer der ehv aue dürfen wieder trainieren
###17:24 | große impfbereitschaft bei helios klinik in aue
###16:30 | schulen im erzgebirge auch kommende woche geöffnet
###16:19 | kritik an verpflichtenden corona-selbsttests an schulen
###16:06 | travestie-künstler jobben als gärtner
###15:50 | nach festival-absagen: mdr plant programm für künstlerinnen und künstler
###14:59 | lange wartezeiten bei kostenlosen schnelltests im landkreis bautzen
###14:46 | vogtlandkreis sieht erfolg bei teststrategie
###14:30 | tourismus-pilotprojekt in augustusburg startet
###14:14 | corona-arbeitsschutzverordnung verlängert
###13:59 | auszahlungen der corona-soforthilfen sollen bald weitergehen
###13:49 | zweites impfzentrum in vogtlandkreis eröffnet
###13:10 | grimma vergibt impftermine an über achtzigjährige
###12:50 | leipzig liest auch ohne buchmesse
###11:34 | wilkau-haßlau und aue-bad schlema verlangen keine gebühr für stühle im freien oder werbeaufsteller
###11:00 | sundair will ab mai wieder von dresden und leipzig halle starten
###10:25 | kritik an auszahlungsstopp bei corona-soforthilfen
###10:00 | mdrfragt: mehrheit glaubt nicht an krisenende in diesem jahr
###09:50 | stiko-chef rechnet mit eu-zulassung für sputnik v
###08:35 | spahn gibt grünes licht für impffreigabe im vogtland
###08:22 | oberlausitzer dampflok geht auf geisterfahrt
###07:55 | weniger erkältungen: absatz von papiertaschentüchern sinkt
###06:59 | leipzig erlaubt öffnung von kosmetik- und nagelstudios
###06:38 | sporttrainer stellt übungsvideos für kids und eltern ins netz
###06:00 | leipziger eisenbahntage ende märz abgesagt
###05:55 | schnelltest-konzept im kreis bautzen ruckelt noch
###05:44 | mehr als 600 menschen kommen zum massentest
###05:36 | friseurtermine bleiben gefragt - azubis fehlen modelle
###05:25 | chemnitz bereitet öffnungen vor - viele fragen noch offen
