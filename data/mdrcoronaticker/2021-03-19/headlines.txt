###22:06 | tickerende
###21:10 | polen als hochindenzgebiet eingestuft
###20:49 | impfstart in hausarztpraxen am 5.april
###20:17 | chemnitzer kunstsammlungen bleiben zu
###19:50 | eventim-chef: kein normaler veranstaltungsbetrieb im sommer
###18:56 | erneut vierstelliger neuinfektionswert
###18:26 | zusätzlicher biontech-impfstoff für sachsen
###18:05 | illegale gäste ohne maske in riesaer lokal
###17:45 | kretschmer schließt weitere corona-lockerungen vorerst aus
###17:00 | freiwillige corona-tests im vogtland boomen
###16:42 | spd verschiebt landes-parteitag wegen corona
###16:33 | 168 infektionen bei selbsttests von schülern entdeckt
###16:18 | landkreis bautzen plant mit 48 corona-testzentren
###14:55 | dresdner corona-testzentrum zieht in den kulturpalast
###14:09 | sachsens kulturbranche demonstriert vor dem landtag
###13:46 | landkreis meißen schließt kitas und schulen
###13:41 | 13 corona-fälle im jugendgefängnis regis-breitingen
###13:03 | steuereinnahmen erneut gesunken
###12:45 | neue testzentren in wilkau-haßlau und in chemnitz
###12:23 | drk impft ab morgen wieder mit astrazeneca
###11:28 | borsdorf will eigenes testzentrum aufmachen
###11:06 | plauen verschiebt museumsnacht ins nächste jahr
###10:56 | bald wieder besucherinnen und besucher in oberwiesenthal.
###09:50 | falkenstein kurbelt lokalen handel mit falkenscheinen an
###09:14 | infektionszahlen steigen weiter
###08:59 | thallwitz plant eigenes schnelltestzentrum
###08:24 | chorgesänge in leipziger thomaskirche
###07:43 | impfungen mit astrazeneca werden fortgesetzt
###07:11 | landrat emanuel: fixieren auf inzidenzwerte dringend überdenken
###06:30 | erste jugendweihefeiern verschoben
###06:06 | auftrittsmöglichkeiten für musiknachwuchs
###05:32 | kommunen fordern früheres impfen in arztpraxen
###05:21 | köpping dankt allen beschäftigten des öffentlichen gesundheitsdienstes
###05:08 | kultur- und veranstaltungsbranche protestiert in dresden
