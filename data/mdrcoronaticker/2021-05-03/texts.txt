an dieser stelle beenden wir den corona-ticker aus sachsen für heute. wir bedanken uns fürs lesen und wünschen ihnen einen schönen abend. wir sind am dienstag wieder für sie da. bleiben sie gesund.
es gibt studien, wonach das risiko für einen schweren covid-19-verlauf bei schwangeren erhöht ist. dennoch wird diese gruppe in deutschland bislang nur in ausnahmefällen geimpft.
seit monaten debattieren politiker, eltern und bildungsvertreter über kinder und jugendliche, welche spuren die pandemie in ihrem leben hinterlassen wird. mdr sachsen hat persönlich nachgefragt. bei der 15 jahre alten dresdnerin luzia manuwald und beim neunjährigen paul wachler aus leipzig und seiner schwester karla.
eigentlich sollte in sachsen ab dem 3.mai ein härtefallfonds für coronageschädigte firmen aufgelegt werden. die gelder waren für unternehmen gedacht, die bei den corona-hilfen bisher leer ausgegangen sind. doch daraus wird erstmal nichts. wie das wirtschaftsministerium auf anfrage mitteilte, verschiebt sich der start des neuen fonds. man müsse auf den bund warten, hieß es aus dem ministerium.
der entwurf der neuen sächsischen corona-schutzverordnung sieht eine vereinfachung der corona-regeln sowie erleichterungen für genesene und geimpfte vor. das berichtet die leipziger volkszeitung (lvz) unter berufung auf den entwurf, der am dienstag im kabinett verabschiedet werden soll. demnach sollen ab einer unterschreitung der 7-tage-inzidenz von 100 an fünf aufeinanderfolgenden tagen verschiedene lockerungen in kraft treten. so seien ab dann beispielsweise treffen mit fünf personen aus zwei hausständen erlaubt. auch die ausgangsbeschränkungen würden abgeschafft. ebenfalls sollen baumärkte und fahrzeug- und fahrradhändler dann wieder öffnen dürfen. für geimpfte (ab 14 tagen nach der zweiten impfung) und genesene soll die in vielen bereichen vorgeschriebene testpflicht entfallen.
die bundespolizei leipzig hat am sonnabend ein treffen von mehreren jugendlichen am bahnhof wurzen aufgelöst. diese hatten sich auf dem bahngelände getroffen, wie die bundespolizei mitteilte. nach der feststellung der personalien habe man die zehn jugendlichen über die gefahren auf bahnanlagen und ihre verstöße gegen die corona-schutzverordnung aufgeklärt. gegen die jugendlichen wurden ordnungswidrigkeitenverfahren eingeleitet.
bei den vektorimpfstoffen von astrazeneca und johnson &amp; johnson kommt es in seltenen fällen zu einer autoimmunreaktion, die zu schweren thrombosen führt. die exakte ursache ist noch unbekannt. forscher vermuten, dass es sich um einen klasseneffekt dieser vektorbasierten impfstoffe handelt.
die landkreise und kreisfreien städte in sachsen haben am montag insgesamt 528 corona-neuinfektionen gemeldet. laut robert-koch-institut liegt die 7-tage-inzidenz für den freistaat bei 216. damit liegt sie deutlich über dem bundesschnitt von 147. in zwei landkreisen liegt die inzidenz aktuell bei bei über 300. der landkreis mittelsachsen meldet mit 371 den höchsten wert im freistaat, die stadt leipzig mit 115 den niedrigsten. insgesamt sind 34 weitere menschen im zusammenhang mit einer corona-infektion verstorben. damit erhöht sich die zahl der todesfälle in der pandemie in sachsen auf 9.243.
das drk sachsen hat angekündigt, ab 19 uhr 3.800 impftermine für die impfzentren in dresden, chemnitz, leipzig, löbau, riesa und zwickau freizuschalten. weitere termine sollen ab mittwoch folgen, so das drk beim kurznachrichtendienst twitter. die termine seien nur online buchbar und nicht per telefon.
trotz der corona-pandemie haben die exporte der sächsischen wirtschaft im januar und februar wieder deutlich zugelegt. mit einem gesamtwert von 6,8 milliarden euro lag der exportumsatz um sieben prozent über dem des vorjahreszeitraumes, wie das statistische landesamt am montag in kamenz mitteilte. damit legte der export wieder zu, nachdem die jahresbilanz 2020 mit einem gesamtumsatz von 36,8 milliarden euro um fast neun prozent unter dem ergebnis des jahres 2019 gelegen hatte.die ausfuhr in die europäischen länder stieg den angaben zufolge in den ersten zwei monaten des jahres um neun prozent auf knapp vier milliarden euro. besonders bei den lieferungen nach frankreich und italien hätten mit 27 beziehungsweise 22 prozent bedeutende zuwächse festgestellt werden können. dagegen sei die ausfuhr in die von der corona-krise stark betroffenen länder tschechien und polen von rückgängen von acht und zehn prozent gegenüber den vorjahresmonaten betroffen gewesen.
die lange nacht der kunst, kultur und architektur in meißen ist erneut verschoben worden. nach dem aktuellen stand der bestimmungen sei der termin am 3.juli nicht zu halten, sagte kulturreferentin sara engelmann. sie hoffe auf einen nachholtermin im spätsommer. darüber wolle man im juni entscheiden. 2020 musste die lange nacht coronabedingt abgesagt werden.
bundesgesundheitsminister jens spahn hat rasche erleichterungen für vollständig geimpfte angekündigt. bundestag und bundesrat sollten dafür noch diese, spätestens aber kommende woche eine verordnung beschließen, sagte der cdu-politiker nach einer kabinettssitzung. demnach sollen vollständig geimpfte mit tagesaktuell getesteten und genesenen gleichgestellt werden.
voraussichtlich ab 10.mai sollen leipzigs sportvereinen kostenlos testkits zur verfügung gestellt werden. geplant ist die unterstützung zunächst für vier wochen, wie das sportamt der stadt mitteilte. für die beschaffung der tests würden 25.000 euro aus der finanziellen sportförderung übertragen. laut aktueller corona-schutzverordnung des freistaates können sportvereine kontaktfreien sport in gruppen von bis zu fünf kindern bis zur vollendung des 14. lebensjahres anbieten. jedoch muss in diesem fall das trainingspersonal einen tagesaktuellen negativen test vorweisen können , sagte katja büchel, leiterin des amtes für sport.
die volkshochschule dresden bietet gemeinsam mit der social web macht schule g gmb h im mai onlineveranstaltungen an, die lehrern und lehrerinnen fit im umgang mit neuen medien machen sollen. das lehrpersonal müsse sich nicht nur pandemiebedingt verschiedenen herausforderungen des medieneinsatzes stellen, teilte die vhs mit. auch grundsätzlich habe sich das anforderungsprofil in bezug auf die vermittlung von lerninhalten in den letzten jahren stark verändert. allerdings werde gerade in der aktuell herrschenden pandemiesituation deutlich, welch großen stellenwert digitale medien im schulalltag haben.die vorträge finden online statt. weitere informationen und die möglichkeit zur anmeldung gibt es montags bis freitags jeweils von 9 uhr bis 13 uhr telefonisch unter 0351-254400 sowie rund um die uhr über die homepage.
unbekannte haben aus einer impfstation in st. egidien im landkreis zwickau eine ampulle mit sieben dosen corona-impfstoff gestohlen. die diebe hätten am sonntag einen unbeobachteten moment ausgenutzt und den impfstoff des herstellers biontech aus einem unverschlossenen arztzimmer entwendet, teilte die polizei mit. sie sucht nun mögliche zeugen, denen im umfeld der impfstation personen aufgefallen sind, die sich verdächtig verhalten haben.
die deutschen werkstätten hellerau haben mit corona-impfungen für mitarbeitende begonnen. am vergangenen sonnabend seien die ersten 60 beschäftigten mit astrazeneca geimpft worden, sagte eine sprecherin des unternehmens. in kooperation mit einem oberlausitzer ärztepaar habe man in den unternehmensräumen dafür anmeldung, wartebereich sowie einen raum für die beratungsgespräche und impfungen eingerichtet. den angaben zufolge wurde innerhalb von fünf stunden 60 mitarbeitern eine spritze verabreicht. das interesse sei groß gewesen. drei stunden nach bekanntgabe des termins waren alle impftermine ausgebucht. an diesem wochenende sollen daher erneut 150 menschen in den räumen der deutschen werkstätten geimpft werden.
obwohl in den großstädten in sachsen, sachsen-anhalt und thüringen mehr menschen wohnen als auf dem land, ist der anteil der neuinfektionen in zentren wie dresden, magdeburg oder erfurt geringer als im umland. warum.
das robert-koch-institut hat gemeinsam mit mehreren gesetzlichen krankenkassen und forschungsinstituten ein projekt zur identifizierung von risikofaktoren für schwere covid-19-verläufe begonnen. ziel des projektes ist es herauszufinden, welche vorerkrankungen bei personen unter 80 jahren besonders häufig mit einem schweren covid-19-verlauf einhergehen, teilte die aok mit. besonderes augenmerk liege auf vorerkrankungen, die vergleichsweise häufig zu einer behandlung auf einer intensivstation, zur beatmung oder sogar dem tod führen können.in der studie würden abrechnungsdaten von über 30 millionen gesetzlich versicherten untersucht. ziel sei auch, erkenntnisse zur dringlichkeit der corona-schutzimpfung für die verschiedenen erkrankungen abzuleiten.
beim auflösen einer illegalen corona-party sind am sonntagabend in plauen drei polizeibeamte leicht verletzt worden. wie die polizei mitteilte, sei man aufgrund starken lärms zu der feier gerufen worden. als die beamten die identität der fünf feiernden personen feststellen wollte, hätten drei der anwesenden auf die polizisten eingeschlagen. gegen die drei 21, 24 und 43 jahre alten tatverdächtigen wurden anzeigen wegen tätlichen angriffs und widerstands gegen vollstreckungsbeamte in tateinheit mit gefährlicher körperverletzung und sachbeschädigung erstattet, hieß es.
schulschließungen, online-unterricht, quarantäne – familien mit kindern leiden auch in sachsen besonders unter der corona-pandemie. kultusminister christian piwarz will deshalb schnellstmöglich die impfpriorisierung aufheben und ganze familien durchimpfen. der cdu-politiker erwartet davon einen breiten schutz der bevölkerung.
die bundespolizei hat am wochenende an der polnischen grenze mehrere verstöße gegen die corona-einreiseverordnung und gegen die corona-schutzverordnung festgestellt. wie die beamten mitteilten, waren zwei polnische männer an der görlitzer stadtbrücke ihrer testpflicht nicht nachgekommen. drei bulgaren und ein polnischer bürger seien zudem über die bad muskauer brücke aus dem risikogebiet eingereist, hieß es. einerseits konnten die vier reisenden keinen triftigen reisegrund nennen und saßen andererseits ohne mund-nasen-schutz im auto , so die bundespolizei. sie hätten keinen negativ-test mitgeführt. es wurden anzeigen gefertigt und die zuständigen gesundheitsämter verständigt.
auf dem weg zur herdenimmunität und damit zur überwindung der pandemie sind zügige impfungen wichtig. details zur statistik über den impf-fortschritt erklärt mdr wissen.
wer sein kind in die kita bringt, kann nur mit negativem corona-test, der nicht älter als drei tage ist, das gebäude betreten. um den eltern den weg zum test-center zu ersparen, hatten zwei kindertagesstätten in dresden eine idee sie buchten an zwei tagen in der woche eine mobile teststation, die direkt vor der einrichtung corona-schnelltests anbot. weil es eine beschwerde gab, ist der service nicht mehr erlaubt.
der 26. radlersonntag im mülsengrund bei zwickau kann am sonntag wegen der corona-lage nicht stattfinden. mülsens bürgermeister michael franke sagte mdr sachsen, er hoffe auf einen ersatztermin am 29.august - wenn auch nur in abgespeckter version. der radlersonntag alljährlich am muttertag gilt als längstes straßenfest in sachsen zwischen den ortschaften dennheritz und ortsmanndorf. tausende teilnehmer jeden alters radeln im jahreswechsel bergauf- oder bergab durch den mülsengrund. im jahr 2019 musste der radlersonntag wegen bauarbeiten erstmals pausieren, im vorigen jahr war er wegen der corona-pandemie abgesagt worden. franke betonte, das traditionelle radlerfest soll in jedem fall wiederbelebt werden.
die sieben-tage-inzidenz in sachsen ist wieder leicht angestiegen. das robert koch-institut gab den wert heute mit knapp 216 an. am sonntag betrugt er 209. die sieben-tage-inzidenz gibt an, wie viele corona-neuinfektionen es binnen einer woche je 100.000 einwohner gegeben hat.sachsen liegt gleich hinter dem negativ-spitzenreiter unter den bundesländern, hinter thüringen mit 221. corona-hotspots in sachsen sind der kreis mittelsachsen (371) und der erzgebirgskreis (342), gefolgt vom landkreis bautzen (294) und dem landkreis zwickau (274). den niedrigsten wert verzeichnet in sachsen weiterhin die stadt leipzig mit einer inzidenz von knapp 115. laut rki wurden vier neue todesfälle gemeldet. damit sind im freistaat seit beginn der pandemie 9.120 patienten mit oder an den folgen einer corona-erkrankung gestorben.
die gewerkschaft erziehung und wissenschaft fordert zur bewältigung der corona-folgen in sachsens kitas und schulen höhere investitionen des landes. gew-landeschefin uschi kruse sagte, bereits in der frühkindlichen erziehung in kitas sollten fünf prozent mehr erzieherstellen finanziert werden, um krankheitsausfälle kompensieren zu können. dies würde je jahr rund 90 millionen euro kosten, hieß es. für die schulen fordert die gewerkschaft klassenleiterstunden (das heißt de facto mehr lehrerstellen), mehr schulassistenten und mehr schulpsychologen, die jedoch nicht mit geplanten lehrerstellen verrechnet werden dürften. in kürze wird der doppelhaushalt im landtag beraten werden. zudem wird laut gewerkschaft mitte der woche ein programm der bundesbildungsministerin anja karliczek zur überwindung von corona-folgen erwartet.
die deutschen intensivmediziner haben angesichts sinkender infektionszahlen eine erste positive bilanz der bundesweiten corona-notbremse gezogen. wir sind zuversichtlich, dass die zahl der covid-19-patienten auf den intensivstationen sinken wird - und das hängt dann unmittelbar mit den maßnahmen der bundesnotbremse, wie aber auch dem deutlichen fortschritt beim impfen zusammen , sagte der präsident der deutschen interdisziplinären vereinigung für intensiv- und notfallmedizin, gernot marx, der düsseldorfer rheinischen post .die bundesnotbremse hat aus unserer sicht (.. ) viele tausend menschenleben retten können.
der deutsche hausärzteverband fordert rasche impfkonzepte für jüngere. verbandschef ulrich weigeldt sagte dem redaktionsnetzwerk deutschland, viele jüngere menschen rebellierten unter dem eindruck der perspektivlosigkeit. dazu gehörten auch menschen mit migrationshintergrund. sie seien immer weniger bereit, sich an quarantäne- oder hygieneregeln zu halten. viele von ihnen lebten in großen familien unter prekären wohnverhältnissen und hätten ein sehr hohes risiko, das virus weiterzugeben. man müsse diese menschen laut weigeldt mit impfungen motivieren. sie könnten so ihre grundrechte und vor allem ihr leben in freiheit zurückbekommen.
der vorsitzende der deutschen polizeigewerkschaft, rainer wendt, fordert ein bundesweites verbot aller querdenken -demonstrationen. wendt sagte der neuen osnabrücker zeitung , bei diesen kundgebungen sei der rechtsverstoß schon vorprogrammiert. teilnehmer würden keine maske tragen und den mindestabstand nicht einhalten. dagegen müsse man viel rigoroser vorgehen. wenn bereits vorher klar sei, dass die teilnehmer dieser veranstaltungen die auflagen nicht beachten würden, müssten politik und justiz dafür sorgen, dass sie nicht stattfinden, so wendt.
die impfkampagne nimmt auch in sachsen zunehmend an fahrt auf. das drk hat für heute neue impftermine angekündigt. fragen aber bleiben sollen menschen, die bereits eine erkrankung durchgemacht haben, sich impfen lassen - und wenn ja, wann genau nach der genesung. wie lange hält der schutz einer corona-impfung.
gegen 6 45 uhr ist heute in dresden nach sechsmonatiger pause wieder ein linienflug nach amsterdam gestartet. die niederländische klm setzte einen embraer-regionaljet 190 mit 100 sitzplätzen der tochter cityhopper ein. wegen der corona-pandemie war die verbindung den winter über ausgesetzt gewesen. aktuell werden ab sofort drei flüge pro woche angeboten, wobei crew und maschine in dresden übernachten – also abends ankommen und am nächsten tag morgens abfliegen.
die kurt-masur-akademie der dresdner philharmonie will junge musiker zu international anerkannten künstlern formen. neben dem studium treten die nachwuchstalente gemeinsam mit den profis auf. die corona-pandemie fordert von philharmonie und nachwuchsmusikern geduld und flexibilität.
der städte- und gemeindetag und landkreistag in sachsen plädieren für einen anderen umgang mit der pandemie. in einer gemeinsamen teststrategie schlagen sie vor, kontakte zuzulassen und zu öffnen, sofern menschen einen negativen test oder eine immunisierung vorweisen können. der vorschlag sei an den ministerpräsidenten geschickt worden, teilten die verbände mit. im kern gehe es darum, die kontakte weitgehend zuzulassen, sofern die menschen einen tagesaktuellen und negativen corona-test oder bereits eine immunisierung vorweisen können. die derzeit auf bundesebene angestellten überlegungen, negativ getestete und doppelt geimpfte personen gleichzustellen, gehen in die richtige richtung.
heute starten die schulkinowochen sachsen in ihre 14. saison - allerdings nicht in kinosälen, sondern im internet. nachdem die kinowochen im vergangenen jahr wegen der corona-pandemie abgesagt werden mussten, habe man sich in diesem jahr für das online-format entschieden, sagte oliver gibtner-weidlich, der das projekt leitet. insgesamt 26 dokumentar-, spiel- und animationsfilme für verschiedene altersklassen sollen zeitunabhängig nur einen klick entfernt sein. den angaben zufolge werden die filme für schülerinnen, schüler und lehrkräfte bis ende mai als stream auf der website der schulkinowochen zur verfügung stehen, zusammen mit unterrichts- und begleitmaterial. mittlerweile hätten sich bereits rund 6.000 menschen für das programm angemeldet.
der landesschülerrat sachsen fordert vom kultusministerium (smk) eine perspektive für die zeit nach dem lockdown. die bundesnotbremse hat im kultusministerium offenbar eine schockstarre ausgelöst , sagt die vorsitzende des landeschülerrates, joanna kesicka, und fügt an was passiert, wenn landkreise unter die inzidenz von 165 sinken und schulen wieder öffnen. hier muss das smk zeigen, dass es die aktuellen zwangsschließungen nutzt, um für die zeit danach vorzusorgen. unter anderem spricht sich der schülerrat dafür aus, dass alle schüler und lehrer täglich per schnelltest getestet werden und es in den ersten wochen des präsenzunterrichts keine noten gibt.
der botanische garten in chemnitz hat wieder länger geöffnet. wie die stadt mitteilte, können besucher ab sofort sieben tage die woche jeweils von 10 bis 18 uhr vorbeischauen. der letzte einlass ist um 17 uhr. für den besuch muss unter botanischergarten@stadt-chemnitz. de oder der telefonnummer 0371 488 6767 ein termin gebucht werden. außerdem ist ein negativer corona-test notwendig. alle gebäude sind nach wie vor geschlossen. es ist laut stadt auch keine öffentliche toilette vorhanden.
