###21:57 | tickerende
###21:45 | semperoper dresden: hänsel und gretel ab ersten advent im stream
###21:35 | mdr podcast: alarm auf den intensivstationen: kann sachsen die vierte corona-welle brechen.
###21:05 | schneeberg bietet ab nächste woche impfungen hotline ab freitag 8 uhr
###20:52 | landkreis görlitz bereitet verlegung von corona-intensivpatienten vor
###20:01 | impfzentrum auf der messe dresden soll mittwoch starten
###18:54 | schloss wackerbarth startet mit 2g in außengastronomie
###18:40 | landesärztekammer: schlechtere behandlung wegen überlasteter kliniken dresden
###18:35 | corona-pandemie: stadt grimma sucht freiwillige helfer
###18:21 | fußball - fortsetzung des regionalliga-spielbetriebs stößt auf kritik und zustimmung
###18:03 | studie: infektionsschutz der biontech-zweitimpfung nimmt schon nach drei monaten ab
###17:58 | tilgungsfrist für corona-schulden soll verlängert werden
###16:25 | 14 corona-patienten aus sachsen werden in andere bundesländer verlegt
###15:45 | verdi und pflegeverband verteidigen warnstreiks an unikliniken
###14:45 | überlastete praxen: hausärzte fordern strengere corona-regeln
###14:30 | dresden bittet bürger um mithilfe
###13:08 | impfstoff-freigabe für kinder ab fünf jahren in europa
###13:05 | landgericht dresden weist schadenersatz eines leipziger kneipiers ab
###13:00 | ärztinnen und ärzte verabreichen 37.000 corona-schutzimpfungen an einem tag
###12:32 | sachsen stockt impfangebote weiter auf und bittet um geduld
###12:00 | mit dem impfbus in mittelsachsen auf tour
###11:32 | wieder weitere schulen vorübergehend ganz oder teilweise geschlossen
###10:57 | sozialministerin köpping hält lockdown für dringend notwendig
###10:06 | nach zwischenfall wieder impfungen in dresden-prohlis
###08:53 | männelmacher brauchen ideereichtum in der krise
###08:21 | in der regionalliga nordost wieder weiter gekickt
###07:54 | bundesweit 100.000 tote im zusammenhang mit corona
###07:25 | zeitungen: sächsische landesregierung rechnet offenbar mit lockdown
###07:03 | delitzscher oberbürgermeister appelliert an zusammenhalt der bürgerschaft
###06:23 | sachsen beantragt verlegung von corona-intensivpatienten
###06:00 | stiko und gynäkologen raten schwangeren zur impfung
###05:43 | tierheime in sachsen durch corona völlig überfüllt
###05:33 | herzexperte rät jungen männern zu corona-impfung
###05:06 | modellrechnung: so hoch ist das risiko, auf eine infizierte person zu treffen
