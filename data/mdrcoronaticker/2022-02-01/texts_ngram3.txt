,_sagte_der	2
auf_bis_zu	2
der_auslastung_der	2
sozialministerin_petra_köpping	2
petra_köpping_(spd)	2
belastung_für_die	2
eine_neue_corona-notfallverordnung	2
mehr_zuschauer_in	2
in_der_übersicht	2
verstöße_gegen_das	2
gegen_das_versammlungsgesetz	2
gesundheits-_und_pflegebereich	2
nur_in_dänemark	2
in_dänemark_und	2
im_ersten_halbjahr	2
heute_beenden_wir	1
beenden_wir_unseren	1
wir_unseren_ticker	1
unseren_ticker_zur	1
ticker_zur_corona-lage	1
zur_corona-lage_in	1
corona-lage_in_sachsen.	1
in_sachsen._morgen	1
sachsen._morgen_früh	1
morgen_früh_sind	1
früh_sind_wir	1
sind_wir_mit	1
wir_mit_neuen	1
mit_neuen_meldungen	1
neuen_meldungen_zurück.	1
meldungen_zurück._wir	1
zurück._wir_wünschen	1
wir_wünschen_ihnen	1
wünschen_ihnen_einen	1
ihnen_einen_schönen	1
einen_schönen_abend	1
schönen_abend_und	1
abend_und_eine	1
und_eine_geruhsame	1
|_innenminister_zum	1
innenminister_zum_umgang	1
zum_umgang_mit	1
umgang_mit_demokratiefeindlichen	1
versammlungsrecht_ist_ein	1
ist_ein_hohes,	1
ein_hohes,_verfassungsrechtlich	1
hohes,_verfassungsrechtlich_garantiertes	1
verfassungsrechtlich_garantiertes_gut.	1
garantiertes_gut._es	1
gut._es_ist	1
es_ist_legitim,	1
ist_legitim,_auch	1
legitim,_auch_in	1
auch_in_zeiten	1
in_zeiten_der	1
zeiten_der_pandemie	1
der_pandemie_protest	1
pandemie_protest_zu	1
protest_zu_äußern.	1
zu_äußern._nicht	1
äußern._nicht_akzeptabel	1
nicht_akzeptabel_ist	1
akzeptabel_ist_jedoch,	1
ist_jedoch,_dass	1
jedoch,_dass_sich	1
dass_sich_zahlreiche	1
sich_zahlreiche_bürgerinnen	1
zahlreiche_bürgerinnen_und	1
bürgerinnen_und_bürger	1
und_bürger_den	1
bürger_den_aufrufen	1
den_aufrufen_von	1
aufrufen_von_rechtsextremistischen	1
von_rechtsextremistischen_initiatoren	1
rechtsextremistischen_initiatoren_anschließen	1
initiatoren_anschließen_und	1
anschließen_und_damit	1
und_damit_für	1
damit_für_deren	1
für_deren_verfassungsfeindliche	1
deren_verfassungsfeindliche_ziele	1
verfassungsfeindliche_ziele_instrumentalisiert	1
ziele_instrumentalisiert_werden.	1
instrumentalisiert_werden._das	1
werden._das_sagten	1
das_sagten_die	1
sagten_die_beiden	1
die_beiden_innenminister	1
beiden_innenminister_von	1
innenminister_von_sachsen	1
von_sachsen_und	1
sachsen_und_thüringen	1
und_thüringen_bei	1
thüringen_bei_einem	1
bei_einem_gemeinsamen	1
einem_gemeinsamen_treffen.	1
gemeinsamen_treffen._roland	1
treffen._roland_wöller	1
roland_wöller_und	1
wöller_und_georg	1
und_georg_maier	1
georg_maier_appellierten	1
maier_appellierten_an	1
appellierten_an_alle	1
an_alle_friedlich	1
alle_friedlich_demonstrierenden,	1
friedlich_demonstrierenden,_sich	1
demonstrierenden,_sich_nicht	1
sich_nicht_vor	1
nicht_vor_den	1
vor_den_karren	1
den_karren_bspw.	1
karren_bspw._der	1
bspw._der_afd	1
der_afd_oder	1
afd_oder_der	1
oder_der_freien	1
der_freien_sachsen	1
freien_sachsen_spannen	1
sachsen_spannen_zu	1
spannen_zu_lassen.machen	1
zu_lassen.machen_sie	1
lassen.machen_sie_sich	1
sie_sich_nicht	1
sich_nicht_mit	1
nicht_mit_denen	1
mit_denen_gemein,	1
denen_gemein,_die	1
gemein,_die_nicht	1
die_nicht_für	1
nicht_für_ihr	1
für_ihr_anliegen	1
ihr_anliegen_stehen,	1
anliegen_stehen,_sondern	1
stehen,_sondern_die	1
sondern_die_pandemielage	1
die_pandemielage_nutzen,	1
pandemielage_nutzen,_um	1
nutzen,_um_verschwörungstheorien	1
um_verschwörungstheorien_und	1
verschwörungstheorien_und_umsturzphantasien	1
und_umsturzphantasien_zu	1
umsturzphantasien_zu_formulieren	1
zu_formulieren_und	1
formulieren_und_unsere	1
und_unsere_demokratie	1
unsere_demokratie_abschaffen	1
demokratie_abschaffen_wollen.	1
abschaffen_wollen._die	1
wollen._die_nicht	1
die_nicht_einmal	1
nicht_einmal_halt	1
einmal_halt_an	1
halt_an_krankenhauseinfahrten	1
an_krankenhauseinfahrten_machen	1
krankenhauseinfahrten_machen_und	1
machen_und_die	1
und_die_in	1
die_in_form	1
in_form_von	1
form_von_persönlichen	1
von_persönlichen_oder	1
persönlichen_oder_über	1
oder_über_das	1
über_das_internet	1
das_internet_verbreiteten	1
internet_verbreiteten_tiraden	1
verbreiteten_tiraden_politikerinnen	1
tiraden_politikerinnen_und	1
politikerinnen_und_politiker	1
und_politiker_bedrohen,	1
politiker_bedrohen,_bis	1
bedrohen,_bis_hin	1
bis_hin_zum	1
|_unikliniken-chefs_sehen	1
unikliniken-chefs_sehen_impfpflicht	1
beiden_universitätskliniken_in	1
universitätskliniken_in_sachsen	1
in_sachsen_befürchten	1
sachsen_befürchten_keinen	1
befürchten_keinen_aderlass	1
keinen_aderlass_bei	1
aderlass_bei_ihrem	1
bei_ihrem_pflegepersonal	1
ihrem_pflegepersonal_durch	1
pflegepersonal_durch_die	1
durch_die_beschlossene	1
die_beschlossene_einrichtungsbezogene	1
beschlossene_einrichtungsbezogene_corona-impfpflicht.	1
einrichtungsbezogene_corona-impfpflicht._es	1
corona-impfpflicht._es_sei	1
es_sei_ein	1
sei_ein_problem,	1
ein_problem,_aber	1
problem,_aber_keine	1
aber_keine_gefahr	1
keine_gefahr_,	1
gefahr_,_sagte	1
sagte_der_medizinische	1
der_medizinische_vorstand	1
medizinische_vorstand_der	1
vorstand_der_universitätsklinik	1
der_universitätsklinik_leipzig	1
universitätsklinik_leipzig_,	1
leipzig_,_christoph	1
,_christoph_josten,	1
christoph_josten,_der	1
josten,_der_deutschen	1
der_deutschen_presse-agentur.	1
deutschen_presse-agentur._die	1
presse-agentur._die_impfquote	1
die_impfquote_liege	1
impfquote_liege_bei	1
liege_bei_ärztinnen	1
bei_ärztinnen_und	1
ärztinnen_und_ärzten	1
und_ärzten_bei	1
ärzten_bei_mehr	1
bei_mehr_als	1
mehr_als_90	1
als_90_prozent	1
90_prozent_und	1
prozent_und_beim	1
und_beim_medizinischen	1
beim_medizinischen_personal	1
medizinischen_personal_bei	1
personal_bei_80	1
bei_80_prozent.	1
80_prozent._es	1
prozent._es_gebe	1
es_gebe_aber	1
gebe_aber_auch	1
aber_auch_signale	1
auch_signale_von	1
signale_von_mitarbeitern,	1
von_mitarbeitern,_die	1
mitarbeitern,_die_dann	1
die_dann_gehen	1
dann_gehen_wollten.der	1
gehen_wollten.der_medizinische	1
wollten.der_medizinische_vorstand	1
medizinische_vorstand_des	1
vorstand_des_universitätsklinikums	1
des_universitätsklinikums_dresden	1
universitätsklinikums_dresden_,	1
dresden_,_michael	1
,_michael_albrecht,	1
michael_albrecht,_rät	1
albrecht,_rät_dazu	1
rät_dazu_abzuwarten.	1
dazu_abzuwarten._in	1
abzuwarten._in_frankreich	1
in_frankreich_und	1
frankreich_und_anderen	1
und_anderen_ländern	1
anderen_ländern_habe	1
ländern_habe_am	1
habe_am_ende	1
am_ende_kaum	1
ende_kaum_einer	1
kaum_einer_seinen	1
einer_seinen_job	1
seinen_job_dafür	1
job_dafür_aufgegeben.	1
dafür_aufgegeben._albrecht	1
aufgegeben._albrecht_geht	1
albrecht_geht_nicht	1
geht_nicht_von	1
nicht_von_einer	1
von_einer_riesigen	1
einer_riesigen_kündigungswelle	1
riesigen_kündigungswelle_aus.	1
kündigungswelle_aus._um	1
aus._um_die	1
um_die_80	1
die_80_prozent	1
80_prozent_der	1
prozent_der_pflegekräfte	1
der_pflegekräfte_seien	1
pflegekräfte_seien_geimpft.	1
seien_geimpft._die	1
geimpft._die_impfablehnungsquoten	1
die_impfablehnungsquoten_von	1
impfablehnungsquoten_von_20	1
von_20_bis	1
20_bis_30	1
bis_30_prozent	1
30_prozent_sei	1
prozent_sei_relativ	1
sei_relativ_typisch	1
relativ_typisch_für	1
typisch_für_sachsen.	1
für_sachsen._die	1
sachsen._die_quote	1
die_quote_bei	1
quote_bei_den	1
bei_den_ärzten	1
den_ärzten_liegt	1
ärzten_liegt_laut	1
liegt_laut_albrecht	1
laut_albrecht_bei	1
albrecht_bei_96	1
|_kurzarbeiterregeln_bis	1
kurzarbeiterregeln_bis_30.juni	1
hubertus_heil_(spd)	1
heil_(spd)_hat	1
(spd)_hat_heute	1
hat_heute_angekündigt,	1
heute_angekündigt,_den	1
angekündigt,_den_vereinfachten	1
den_vereinfachten_zugang	1
vereinfachten_zugang_zum	1
zugang_zum_kurzarbeitergeld	1
zum_kurzarbeitergeld_bis	1
kurzarbeitergeld_bis_zum	1
bis_zum_30.juni	1
zum_30.juni_2022	1
30.juni_2022_zu	1
2022_zu_verlängern.	1
zu_verlängern._verlängert	1
verlängern._verlängert_wird	1
verlängert_wird_auch	1
wird_auch_die	1
auch_die_höchstbezugsdauer	1
die_höchstbezugsdauer_für	1
höchstbezugsdauer_für_kurzarbeitergeld	1
für_kurzarbeitergeld_von	1
kurzarbeitergeld_von_24	1
von_24_auf	1
24_auf_bis	1
bis_zu_28	1
zu_28_monate.	1
28_monate._das	1
monate._das_kurzarbeitergeld	1
das_kurzarbeitergeld_kann	1
kurzarbeitergeld_kann_ab	1
kann_ab_dem	1
ab_dem_4.	1
dem_4._und	1
4._und_7.	1
und_7._bezugsmonat	1
7._bezugsmonat_aufgestockt	1
bezugsmonat_aufgestockt_werden.	1
aufgestockt_werden._sachsens	1
werden._sachsens_wirtschafts-	1
sachsens_wirtschafts-_und	1
wirtschafts-_und_arbeitsminister	1
und_arbeitsminister_martin	1
arbeitsminister_martin_dulig	1
martin_dulig_(spd)	1
dulig_(spd)_begrüßte	1
(spd)_begrüßte_den	1
|_bistum_und	1
bistum_und_bundesligist	1
und_bundesligist_rb	1
katholische_bistum_dresden-meißen	1
bistum_dresden-meißen_und	1
dresden-meißen_und_den	1
und_den_bundesligist	1
den_bundesligist_rb	1
bundesligist_rb_leipzig	1
rb_leipzig_eint	1
leipzig_eint_ihre	1
eint_ihre_unzufriedenheit	1
ihre_unzufriedenheit_mit	1
unzufriedenheit_mit_den	1
mit_den_geänderten	1
den_geänderten_coronaregeln	1
geänderten_coronaregeln_ab	1
coronaregeln_ab_6.februar.	1
ab_6.februar._beide	1
6.februar._beide_institutionen	1
beide_institutionen_kritisierten	1
institutionen_kritisierten_die	1
kritisierten_die_regeln	1
die_regeln_,	1
regeln_,_die	1
,_die_ihnen	1
die_ihnen_nicht	1
ihnen_nicht_weit	1
nicht_weit_genug	1
weit_genug_gehen.	1
genug_gehen._50	1
gehen._50_statt	1
50_statt_20	1
statt_20_gäste	1
20_gäste_bei	1
gäste_bei_trauungen	1
bei_trauungen_oder	1
trauungen_oder_beerdigungen	1
oder_beerdigungen_seien	1
beerdigungen_seien_ein	1
seien_ein_erster	1
ein_erster_schritt.	1
erster_schritt._trotzdem	1
schritt._trotzdem_ist	1
trotzdem_ist_eine	1
ist_eine_absolute	1
eine_absolute_zahl	1
absolute_zahl_nicht	1
zahl_nicht_verhältnismäßig	1
nicht_verhältnismäßig_,	1
verhältnismäßig_,_sagte	1
sagte_der_leiter	1
der_leiter_des	1
leiter_des_bistums-krisenstabs,	1
des_bistums-krisenstabs,_samuel-kim	1
bistums-krisenstabs,_samuel-kim_schwope.	1
samuel-kim_schwope._das	1
schwope._das_bistum	1
das_bistum_fände	1
bistum_fände_es	1
fände_es_besser,	1
es_besser,_teilnehmerzahlen	1
besser,_teilnehmerzahlen_ausschließlich	1
teilnehmerzahlen_ausschließlich_nach	1
ausschließlich_nach_mindestabstand	1
nach_mindestabstand_und	1
mindestabstand_und_raumgröße	1
und_raumgröße_zu	1
raumgröße_zu_bemessen.auch	1
zu_bemessen.auch_rb	1
bemessen.auch_rb_leipzig	1
rb_leipzig_sieht	1
leipzig_sieht_in	1
sieht_in_der	1
in_der_erweiterung	1
der_erweiterung_der	1
erweiterung_der_auslastung	1
auslastung_der_stadien	1
der_stadien_auf	1
stadien_auf_bis	1
bis_zu_25	1
zu_25_prozent,	1
25_prozent,_statt	1
prozent,_statt_wie	1
statt_wie_bisher	1
wie_bisher_nur	1
bisher_nur_1.000	1
nur_1.000_zuschauer,	1
1.000_zuschauer,_nur	1
zuschauer,_nur_einen	1
nur_einen_ersten	1
einen_ersten_schritt.	1
ersten_schritt._wir	1
schritt._wir_sind	1
wir_sind_angesichts	1
sind_angesichts_der	1
angesichts_der_tatsache,	1
der_tatsache,_dass	1
tatsache,_dass_wir	1
dass_wir_in	1
wir_in_sachsen	1
in_sachsen_derzeit	1
sachsen_derzeit_bei	1
derzeit_bei_der	1
bei_der_entscheidenden	1
der_entscheidenden_kennziffer,	1
entscheidenden_kennziffer,_der	1
kennziffer,_der_auslastung	1
auslastung_der_krankenhausbetten,	1
der_krankenhausbetten,_sogar	1
krankenhausbetten,_sogar_unterhalb	1
sogar_unterhalb_der	1
unterhalb_der_sogenannten	1
der_sogenannten_vorwarnstufe	1
sogenannten_vorwarnstufe_liegen,	1
vorwarnstufe_liegen,_nicht	1
liegen,_nicht_zufrieden	1
nicht_zufrieden_mit	1
zufrieden_mit_der	1
mit_der_entscheidung	1
der_entscheidung_des	1
entscheidung_des_kabinetts	1
des_kabinetts_,	1
kabinetts_,_teilte	1
,_teilte_rb	1
|_100.000_ungeimpfte	1
100.000_ungeimpfte_im	1
ungeimpfte_im_pflege-	1
im_pflege-_und	1
medizin-_und_pflegebereich	1
und_pflegebereich_sachsens	1
pflegebereich_sachsens_arbeiten	1
sachsens_arbeiten_rund	1
arbeiten_rund_300.00	1
rund_300.00_menschen.	1
300.00_menschen._davon	1
menschen._davon_sind	1
davon_sind_nach	1
sind_nach_worten	1
nach_worten_von	1
worten_von_sozialministerin	1
von_sozialministerin_petra	1
köpping_(spd)_100.000	1
(spd)_100.000_nicht	1
100.000_nicht_geimpft.	1
nicht_geimpft._um	1
geimpft._um_diese	1
um_diese_beschäftigten	1
diese_beschäftigten_müssten	1
beschäftigten_müssten_sich	1
müssten_sich_die	1
sich_die_gesundheitsämter	1
die_gesundheitsämter_kümmern,	1
gesundheitsämter_kümmern,_wenn	1
kümmern,_wenn_wegen	1
wenn_wegen_der	1
wegen_der_einrichtungsbzogenen	1
der_einrichtungsbzogenen_impfpflicht	1
einrichtungsbzogenen_impfpflicht_betretungs-	1
impfpflicht_betretungs-_und	1
betretungs-_und_beschäftigungsverbote	1
und_beschäftigungsverbote_ausgesprochen	1
beschäftigungsverbote_ausgesprochen_werden	1
ausgesprochen_werden_müssten.	1
werden_müssten._das	1
müssten._das_ist	1
das_ist_wirklich	1
ist_wirklich_eine	1
wirklich_eine_enorme	1
eine_enorme_belastung	1
enorme_belastung_für	1
für_die_gesundheitsämter	1
die_gesundheitsämter_,	1
gesundheitsämter_,_sagte	1
,_sagte_köpping.sie	1
sagte_köpping.sie_kündigte	1
köpping.sie_kündigte_an,	1
kündigte_an,_dass	1
an,_dass_um	1
dass_um_den	1
um_den_11.februar	1
den_11.februar_eine	1
11.februar_eine_handreichung	1
eine_handreichung_für	1
handreichung_für_betroffene	1
für_betroffene_firmen	1
betroffene_firmen_und	1
firmen_und_pflegeeinerichtungen	1
und_pflegeeinerichtungen_zur	1
pflegeeinerichtungen_zur_einrichtungsbezogenen	1
zur_einrichtungsbezogenen_impfpflicht	1
einrichtungsbezogenen_impfpflicht_vorgelegt	1
impfpflicht_vorgelegt_werde.	1
vorgelegt_werde._zudem	1
werde._zudem_versprach	1
zudem_versprach_sie,	1
versprach_sie,_dass	1
sie,_dass_regeln	1
dass_regeln_getroffen	1
regeln_getroffen_würden,	1
getroffen_würden,_damit	1
würden,_damit_die	1
damit_die_versorgung	1
die_versorgung_in	1
versorgung_in_der	1
in_der_pflege	1
der_pflege_gesichert	1
pflege_gesichert_bleibe.es	1
gesichert_bleibe.es_wäre	1
bleibe.es_wäre_mir	1
wäre_mir_viel	1
mir_viel_lieber,	1
viel_lieber,_wenn	1
lieber,_wenn_noch	1
wenn_noch_viel	1
noch_viel_mehr	1
viel_mehr_menschen	1
mehr_menschen_zur	1
menschen_zur_impfung	1
|_sachsen_lockert	1
sachsen_lockert_die	1
lockert_die_coronaregeln	1
die_coronaregeln_ab	1
regierung_hat_eine	1
hat_eine_neue	1
neue_corona-notfallverordnung_beschlossen.	1
corona-notfallverordnung_beschlossen._darin	1
beschlossen._darin_sind	1
darin_sind_unter	1
sind_unter_anderem	1
unter_anderem_lockerungen	1
anderem_lockerungen_für	1
lockerungen_für_hochzeiten	1
für_hochzeiten_vorgesehen,	1
hochzeiten_vorgesehen,_messen	1
vorgesehen,_messen_sollen	1
messen_sollen_unter	1
sollen_unter_2g+	1
unter_2g+_stattfinden	1
2g+_stattfinden_können	1
stattfinden_können_und	1
können_und_mehr	1
und_mehr_zuschauer	1
zuschauer_in_stadien	1
in_stadien_gehen.	1
stadien_gehen._die	1
gehen._die_eckpunkte	1
die_eckpunkte_in	1
eckpunkte_in_der	1
der_übersicht_von	1
übersicht_von_mdr	1
|_einsatzbilanz_der	1
einsatzbilanz_der_polizei	1
der_polizei_zu	1
sachsen_haben_sich	1
haben_sich_am	1
sich_am_montagabend	1
am_montagabend_rund	1
montagabend_rund_5.500	1
rund_5.500_menschen	1
5.500_menschen_bei	1
menschen_bei_rund	1
bei_rund_200	1
rund_200_sogenannten	1
200_sogenannten_spaziergängen	1
sogenannten_spaziergängen_versammelt.	1
spaziergängen_versammelt._knapp	1
versammelt._knapp_2.000	1
knapp_2.000_polizistinnen	1
2.000_polizistinnen_und	1
polizistinnen_und_polizisten	1
und_polizisten_hätten	1
polizisten_hätten_die	1
hätten_die_versammlungen	1
die_versammlungen_abgesichert,	1
versammlungen_abgesichert,_sagte	1
abgesichert,_sagte_innenminister	1
sagte_innenminister_roland	1
innenminister_roland_wöller	1
roland_wöller_(cdu).	1
wöller_(cdu)._gestern	1
(cdu)._gestern_seien	1
gestern_seien_123	1
seien_123_verstöße	1
123_verstöße_gegen	1
das_versammlungsgesetz_angezeigt	1
versammlungsgesetz_angezeigt_worden	1
angezeigt_worden_sowie	1
worden_sowie_65	1
sowie_65_verstöße	1
65_verstöße_gegen	1
verstöße_gegen_die	1
gegen_die_aktuelle	1
die_aktuelle_corona-notverordnung.die	1
aktuelle_corona-notverordnung.die_gute	1
corona-notverordnung.die_gute_nachricht	1
gute_nachricht_ist	1
nachricht_ist_es	1
ist_es_gab	1
es_gab_keine	1
gab_keine_verletzten	1
keine_verletzten_polizisten.	1
verletzten_polizisten._das	1
polizisten._das_ist	1
das_ist_nach	1
ist_nach_den	1
nach_den_vergangenen	1
den_vergangenen_wochen	1
vergangenen_wochen_keine	1
|_harig_verlangt	1
harig_verlangt_gesetzesänderung	1
verlangt_gesetzesänderung_zur	1
bautzner_landrat_michael	1
landrat_michael_harig	1
michael_harig_(cdu)	1
harig_(cdu)_fordert,	1
(cdu)_fordert,_das	1
fordert,_das_gesetz	1
das_gesetz_zur	1
gesetz_zur_impfpflicht	1
zur_impfpflicht_im	1
impfpflicht_im_gesundheits-	1
im_gesundheits-_und	1
und_pflegebereich_zu	1
pflegebereich_zu_ändern.	1
zu_ändern._harig	1
ändern._harig_sagte	1
harig_sagte_dem	1
sagte_dem_mdr,	1
dem_mdr,_durch	1
mdr,_durch_die	1
durch_die_hochansteckende	1
die_hochansteckende_omikron-variante	1
hochansteckende_omikron-variante_könne	1
omikron-variante_könne_das	1
könne_das_mit	1
das_mit_dieser	1
mit_dieser_impfpflicht	1
dieser_impfpflicht_verbundene	1
impfpflicht_verbundene_ziel	1
verbundene_ziel_nicht	1
ziel_nicht_mehr	1
nicht_mehr_erreicht	1
mehr_erreicht_werden.	1
erreicht_werden._besonders	1
werden._besonders_gefährdete	1
besonders_gefährdete_gruppen	1
gefährdete_gruppen_könnten	1
gruppen_könnten_durch	1
könnten_durch_impfung	1
durch_impfung_nicht	1
impfung_nicht_vor	1
nicht_vor_einer	1
vor_einer_ansteckung	1
einer_ansteckung_geschützt	1
ansteckung_geschützt_werden.	1
geschützt_werden._deshalb	1
werden._deshalb_solle	1
deshalb_solle_die	1
solle_die_bundesregierung	1
die_bundesregierung_über	1
bundesregierung_über_alternativen	1
über_alternativen_nachdenken.	1
alternativen_nachdenken._der	1
nachdenken._der_bundesverband	1
der_bundesverband_der	1
bundesverband_der_ärzte	1
der_ärzte_des	1
ärzte_des_öffentlichen	1
des_öffentlichen_gesundheitsdienstes	1
öffentlichen_gesundheitsdienstes_geht	1
gesundheitsdienstes_geht_davon	1
geht_davon_aus,	1
davon_aus,_dass	1
aus,_dass_die	1
dass_die_gesundheitsämter	1
die_gesundheitsämter_die	1
gesundheitsämter_die_impfpflicht	1
die_impfpflicht_auch	1
impfpflicht_auch_gar	1
auch_gar_nicht	1
gar_nicht_kontrollieren	1
nicht_kontrollieren_können.	1
kontrollieren_können._die	1
können._die_stellvertretende	1
die_stellvertretende_vorsitzende,	1
stellvertretende_vorsitzende,_elke	1
vorsitzende,_elke_bruns-philipps,	1
elke_bruns-philipps,_sagte	1
bruns-philipps,_sagte_der	1
sagte_der_rheinischen	1
der_rheinischen_post	1
rheinischen_post_,	1
post_,_die	1
,_die_prüfung	1
die_prüfung_jedes	1
prüfung_jedes_einzelfalls	1
jedes_einzelfalls_sei	1
einzelfalls_sei_eine	1
sei_eine_erhebliche	1
eine_erhebliche_belastung	1
erhebliche_belastung_für	1
für_die_ämter	1
die_ämter_und	1
ämter_und_zeitnah	1
und_zeitnah_nicht	1
zeitnah_nicht_zu	1
|_caritasdirektor:_fehlende	1
caritasdirektor:_fehlende_impfbereitschaft	1
fehlende_impfbereitschaft_stellt	1
impfbereitschaft_stellt_pflegeeinrichtungen	1
stellt_pflegeeinrichtungen_vor	1
gesundheitsämter_dürfen_nach	1
dürfen_nach_ansicht	1
nach_ansicht_des	1
ansicht_des_caritasdirektors	1
des_caritasdirektors_des	1
caritasdirektors_des_bistums	1
des_bistums_dresden-meißen,	1
bistums_dresden-meißen,_matthias	1
dresden-meißen,_matthias_mitzscherlich,	1
matthias_mitzscherlich,_bei	1
mitzscherlich,_bei_der	1
bei_der_durchsetzung	1
der_durchsetzung_der	1
durchsetzung_der_berufsbezogenen	1
der_berufsbezogenen_impfpflicht	1
berufsbezogenen_impfpflicht_im	1
impfpflicht_im_gesundheitswesen	1
im_gesundheitswesen_die	1
gesundheitswesen_die_versorgungslage	1
die_versorgungslage_nicht	1
versorgungslage_nicht_außer	1
nicht_außer_acht	1
außer_acht_lassen.die	1
acht_lassen.die_gesundheitsämter	1
lassen.die_gesundheitsämter_werden	1
gesundheitsämter_werden_abwägen	1
werden_abwägen_müssen,	1
abwägen_müssen,_was	1
müssen,_was_wichtiger	1
was_wichtiger_ist	1
wichtiger_ist_die	1
ist_die_verlässliche	1
die_verlässliche_versorgung	1
verlässliche_versorgung_pflegebedürftiger	1
versorgung_pflegebedürftiger_menschen	1
pflegebedürftiger_menschen_oder	1
menschen_oder_der	1
oder_der_schutz	1
der_schutz_dieser	1
schutz_dieser_pflegebedürftigen	1
dieser_pflegebedürftigen_vor	1
pflegebedürftigen_vor_nichtgeimpften	1
|_millionen-investition_in	1
millionen-investition_in_leipziger	1
in_leipziger_festivallandschaft	1
festivallandschaft_in_leipzig	1
in_leipzig_soll	1
leipzig_soll_mit	1
soll_mit_millionen-investitionen	1
mit_millionen-investitionen_weiterentwickelt	1
millionen-investitionen_weiterentwickelt_werden.	1
weiterentwickelt_werden._wie	1
werden._wie_die	1
wie_die_stadtverwaltung	1
die_stadtverwaltung_mitteilte,	1
stadtverwaltung_mitteilte,_soll	1
mitteilte,_soll_das	1
soll_das_vorhaben	1
das_vorhaben_mit	1
vorhaben_mit_fördermitteln	1
mit_fördermitteln_in	1
fördermitteln_in_höhe	1
in_höhe_von	1
höhe_von_21	1
von_21_millionen	1
21_millionen_euro	1
millionen_euro_von	1
euro_von_bund	1
von_bund_und	1
bund_und_land	1
und_land_umgesetzt	1
land_umgesetzt_werden.	1
umgesetzt_werden._die	1
werden._die_stadt	1
die_stadt_will	1
stadt_will_das	1
will_das_projekt	1
das_projekt_internationale	1
projekt_internationale_festivallandschaft	1
internationale_festivallandschaft_freie	1
festivallandschaft_freie_szene	1
freie_szene_leipzig	1
szene_leipzig_mit	1
leipzig_mit_weiteren	1
mit_weiteren_2,1	1
weiteren_2,1_millionen	1
2,1_millionen_euro	1
millionen_euro_bezuschussen.	1
euro_bezuschussen._der	1
bezuschussen._der_stadtrat	1
der_stadtrat_soll	1
stadtrat_soll_im	1
soll_im_märz	1
im_märz_über	1
märz_über_einen	1
über_einen_entsprechenden	1
einen_entsprechenden_grundsatzbeschluss	1
entsprechenden_grundsatzbeschluss_entscheiden.	1
grundsatzbeschluss_entscheiden._in	1
entscheiden._in_der	1
in_der_corona-pandemie	1
der_corona-pandemie_leidet	1
corona-pandemie_leidet_die	1
leidet_die_szene	1
die_szene_unter	1
szene_unter_den	1
unter_den_kontakbeschränkungen,	1
den_kontakbeschränkungen,_viele	1
kontakbeschränkungen,_viele_veranstaltung	1
viele_veranstaltung_waren	1
veranstaltung_waren_und	1
waren_und_sind	1
und_sind_nicht	1
sind_nicht_oder	1
nicht_oder_nur	1
oder_nur_mit	1
nur_mit_begrenzter	1
mit_begrenzter_zuschauerzahl	1
|_termine_für	1
termine_für_impfungen	1
für_impfungen_mit	1
impfungen_mit_novavax	1
mit_novavax_ab	1
novavax_ab_mitte	1
sachsen_können_ab	1
können_ab_mitte	1
ab_mitte_des	1
mitte_des_monats	1
des_monats_termine	1
monats_termine_für	1
termine_für_corona-impfungen	1
für_corona-impfungen_mit	1
corona-impfungen_mit_dem	1
mit_dem_neuen	1
dem_neuen_proteinimpfstoff	1
neuen_proteinimpfstoff_von	1
proteinimpfstoff_von_novavax	1
von_novavax_vereinbart	1
novavax_vereinbart_werden.	1
vereinbart_werden._das	1
werden._das_hat	1
das_hat_das	1
hat_das_deutsche	1
das_deutsche_rote	1
deutsche_rote_kreuz	1
rote_kreuz_(drk)	1
kreuz_(drk)_mdr	1
(drk)_mdr_sachsen	1
mdr_sachsen_bestätigt.	1
sachsen_bestätigt._nach	1
bestätigt._nach_angaben	1
nach_angaben_von	1
angaben_von_drk-sprecher	1
von_drk-sprecher_kai	1
drk-sprecher_kai_kranich	1
kai_kranich_wird	1
kranich_wird_das	1
wird_das_online-buchungsportal	1
das_online-buchungsportal_derzeit	1
online-buchungsportal_derzeit_überarbeitet.	1
derzeit_überarbeitet._künftig	1
überarbeitet._künftig_soll	1
künftig_soll_bei	1
soll_bei_der	1
bei_der_terminvereinbarung	1
der_terminvereinbarung_dann	1
terminvereinbarung_dann_ein	1
dann_ein_konkreter	1
ein_konkreter_impfstoff	1
konkreter_impfstoff_ausgewählt	1
impfstoff_ausgewählt_werden	1
ausgewählt_werden_können.	1
werden_können._sachsens	1
können._sachsens_sozialministerin	1
sachsens_sozialministerin_petra	1
petra_köpping_hatte	1
köpping_hatte_erste	1
hatte_erste_novavax-impfungen	1
erste_novavax-impfungen_für	1
novavax-impfungen_für_ende	1
für_ende_februar	1
ende_februar_angekündigt.	1
februar_angekündigt._man	1
angekündigt._man_wolle	1
man_wolle_vor	1
wolle_vor_allem	1
vor_allem_menschen	1
allem_menschen_aus	1
menschen_aus_dem	1
aus_dem_gesundheits-	1
dem_gesundheits-_und	1
und_pflegebereich_erreichen,	1
pflegebereich_erreichen,_die	1
erreichen,_die_bislang	1
die_bislang_noch	1
bislang_noch_nicht	1
noch_nicht_geimpft	1
nicht_geimpft_seien.	1
geimpft_seien._wie	1
seien._wie_viele	1
wie_viele_impfdosen	1
viele_impfdosen_sachsen	1
impfdosen_sachsen_kurzfristig	1
sachsen_kurzfristig_erhalten	1
kurzfristig_erhalten_wird,	1
erhalten_wird,_ist	1
wird,_ist_noch	1
|_kostenlose_tests	1
kostenlose_tests_für	1
tests_für_touristen	1
für_touristen_nur	1
touristen_nur_in	1
die_einen_corona-test	1
einen_corona-test_für	1
corona-test_für_die	1
für_die_reise	1
die_reise_benötigen,	1
reise_benötigen,_können	1
benötigen,_können_sich	1
können_sich_in	1
sich_in_europa	1
in_europa_aktuell	1
europa_aktuell_nur	1
aktuell_nur_in	1
dänemark_und_österreich	1
und_österreich_gratis	1
österreich_gratis_testen	1
gratis_testen_lassen.	1
testen_lassen._darauf	1
lassen._darauf_hat	1
darauf_hat_das	1
hat_das_europäische	1
das_europäische_verbraucherzentrum	1
|_volkshaus_pegau	1
volkshaus_pegau_wird	1
pegau_wird_wieder	1
volkshaus_pegau_im	1
pegau_im_landkreis	1
im_landkreis_leipzig	1
landkreis_leipzig_wird	1
leipzig_wird_ab	1
wird_ab_heute	1
ab_heute_wieder	1
heute_wieder_zum	1
wieder_zum_corona-testzentrum.	1
zum_corona-testzentrum._geöffnet	1
corona-testzentrum._geöffnet_ist	1
geöffnet_ist_montags	1
ist_montags_bis	1
montags_bis_freitags	1
bis_freitags_jeweils	1
freitags_jeweils_von	1
jeweils_von_7	1
von_7_bis	1
7_bis_12	1
bis_12_uhr	1
12_uhr_sowie	1
uhr_sowie_von	1
sowie_von_14	1
von_14_bis	1
14_bis_17	1
bis_17_uhr.	1
17_uhr._leiter	1
uhr._leiter_ralf	1
leiter_ralf_opelt	1
ralf_opelt_sagte,	1
opelt_sagte,_eine	1
sagte,_eine_anmeldung	1
eine_anmeldung_sei	1
anmeldung_sei_nicht	1
sei_nicht_nötig.	1
nicht_nötig._jeder	1
nötig._jeder_könne	1
jeder_könne_einen	1
könne_einen_kostenlosen	1
einen_kostenlosen_schnelltest	1
kostenlosen_schnelltest_kurz	1
schnelltest_kurz_vor	1
kurz_vor_der	1
vor_der_arbeit,	1
der_arbeit,_vor	1
arbeit,_vor_einem	1
vor_einem_pflegeheim-	1
einem_pflegeheim-_oder	1
pflegeheim-_oder_restaurantbesuch	1
oder_restaurantbesuch_erhalten.	1
restaurantbesuch_erhalten._das	1
erhalten._das_volkshaus	1
das_volkshaus_pegau	1
volkshaus_pegau_war	1
pegau_war_bereits	1
war_bereits_im	1
bereits_im_vergangenen	1
im_vergangenen_winter	1
|_horte_in	1
horte_in_borna	1
in_borna_bleiben	1
borna_bleiben_ab	1
bleiben_ab_sofort	1
ab_sofort_die	1
sofort_die_horte	1
die_horte_der	1
horte_der_beiden	1
der_beiden_grundschulen	1
beiden_grundschulen_clemens	1
grundschulen_clemens_thieme	1
clemens_thieme_und	1
thieme_und_kinder	1
und_kinder_dieser	1
kinder_dieser_welt	1
dieser_welt_geschlossen.	1
welt_geschlossen._als	1
geschlossen._als_grund	1
als_grund_nannte	1
grund_nannte_die	1
nannte_die_stadt	1
die_stadt_den	1
stadt_den_personalmangel	1
den_personalmangel_aufgrund	1
personalmangel_aufgrund_des	1
aufgrund_des_pandemiegeschehens.	1
des_pandemiegeschehens._daher	1
pandemiegeschehens._daher_könne	1
daher_könne_auch	1
könne_auch_keine	1
auch_keine_notbetreuung	1
keine_notbetreuung_angeboten	1
notbetreuung_angeboten_werden.	1
angeboten_werden._geplant	1
werden._geplant_ist,	1
geplant_ist,_die	1
ist,_die_einrichtungen	1
die_einrichtungen_am	1
einrichtungen_am_8.februar,	1
am_8.februar,_wieder	1
8.februar,_wieder_zu	1
|_mehr_impfangebote	1
mehr_impfangebote_in	1
coswig_im_landkreis	1
im_landkreis_meißen	1
landkreis_meißen_erweitertder	1
meißen_erweitertder_impfpunkt	1
erweitertder_impfpunkt_im	1
impfpunkt_im_sport-	1
im_sport-_und	1
sport-_und_freizeitzentrum	1
und_freizeitzentrum_olympia	1
freizeitzentrum_olympia_seine	1
olympia_seine_öffnungszeiten.	1
seine_öffnungszeiten._geimpft	1
öffnungszeiten._geimpft_wird	1
geimpft_wird_nun	1
wird_nun_laut	1
nun_laut_rathaus	1
laut_rathaus_dienstag	1
rathaus_dienstag_bis	1
dienstag_bis_donnerstag	1
bis_donnerstag_11	1
donnerstag_11_bis	1
11_bis_18	1
bis_18_uhr	1
18_uhr_freitag	1
uhr_freitag_8	1
freitag_8_bis	1
8_bis_20	1
bis_20_uhr	1
20_uhr_sonnabend	1
uhr_sonnabend_9	1
sonnabend_9_bis	1
9_bis_16	1
bis_16_uhrimpfungen	1
16_uhrimpfungen_sind	1
uhrimpfungen_sind_ohne	1
sind_ohne_termin	1
ohne_termin_oder	1
termin_oder_mit	1
oder_mit_vorheriger	1
mit_vorheriger_terminbuchung	1
vorheriger_terminbuchung_möglich.	1
terminbuchung_möglich._alle	1
möglich._alle_menschen	1
alle_menschen_ab	1
menschen_ab_einem	1
ab_einem_alter	1
einem_alter_von	1
alter_von_fünf	1
von_fünf_jahren	1
fünf_jahren_können	1
jahren_können_geimpft	1
können_geimpft_werden.	1
geimpft_werden._das	1
werden._das_drk	1
das_drk_bietet	1
drk_bietet_noch	1
bietet_noch_einen	1
noch_einen_offenen	1
einen_offenen_impftermin	1
offenen_impftermin_in	1
impftermin_in_einem	1
in_einem_zelt	1
einem_zelt_vor	1
zelt_vor_der	1
vor_der_walzengießerei	1
der_walzengießerei_an	1
walzengießerei_an_-	1
an_-_an	1
-_an_diesem	1
an_diesem_donnerstag	1
diesem_donnerstag_von	1
donnerstag_von_10	1
von_10_bis	1
10_bis_16	1
bis_16_30	1
|_krankenkasse:_sachsen	1
krankenkasse:_sachsen_gehen	1
sachsen_gehen_wieder	1
gehen_wieder_öfter	1
wieder_öfter_zu	1
anhaltender_corona-pandemie_gehen	1
corona-pandemie_gehen_die	1
gehen_die_sachsen	1
die_sachsen_wieder	1
sachsen_wieder_häufiger	1
wieder_häufiger_zur	1
häufiger_zur_krebsvorsorge.	1
zur_krebsvorsorge._im	1
krebsvorsorge._im_ersten	1
ersten_halbjahr_2021	1
halbjahr_2021_wurden	1
2021_wurden_acht	1
wurden_acht_prozent	1
acht_prozent_mehr	1
prozent_mehr_screenings	1
mehr_screenings_durchgeführt	1
screenings_durchgeführt_als	1
durchgeführt_als_im	1
als_im_vorjahreszeitraum,	1
im_vorjahreszeitraum,_teilte	1
vorjahreszeitraum,_teilte_die	1
teilte_die_krankenkasse	1
die_krankenkasse_dak-gesundheit	1
krankenkasse_dak-gesundheit_mit.	1
dak-gesundheit_mit._die	1
mit._die_stärkste	1
die_stärkste_zunahme	1
stärkste_zunahme_bei	1
zunahme_bei_ihren	1
bei_ihren_versicherten	1
ihren_versicherten_gab	1
versicherten_gab_es	1
gab_es_demnach	1
es_demnach_bei	1
demnach_bei_mammographie-untersuchungen	1
bei_mammographie-untersuchungen_-	1
mammographie-untersuchungen_-_die	1
-_die_zahl	1
die_zahl_stieg	1
zahl_stieg_um	1
stieg_um_ein	1
um_ein_viertel	1
ein_viertel_an.	1
viertel_an._auch	1
an._auch_hautchecks	1
auch_hautchecks_nahmen	1
hautchecks_nahmen_um	1
nahmen_um_rund	1
um_rund_zwölf	1
rund_zwölf_prozent	1
zwölf_prozent_zu.	1
prozent_zu._dennoch	1
zu._dennoch_lag	1
dennoch_lag_die	1
lag_die_nutzung	1
die_nutzung_der	1
nutzung_der_krebsvorsorge	1
der_krebsvorsorge_insgesamt	1
krebsvorsorge_insgesamt_noch	1
insgesamt_noch_knapp	1
noch_knapp_15	1
knapp_15_prozent	1
15_prozent_unterhalb	1
prozent_unterhalb_des	1
unterhalb_des_niveaus	1
des_niveaus_vor	1
niveaus_vor_der	1
vor_der_pandemie.	1
der_pandemie._die	1
pandemie._die_krankenkasse	1
die_krankenkasse_hatte	1
krankenkasse_hatte_für	1
hatte_für_ihre	1
für_ihre_analyse	1
ihre_analyse_die	1
analyse_die_zahl	1
die_zahl_der	1
zahl_der_vorsorge-untersuchungen	1
der_vorsorge-untersuchungen_im	1
vorsorge-untersuchungen_im_ersten	1
ersten_halbjahr_2019,	1
halbjahr_2019,_2020	1
2019,_2020_und	1
2020_und_2021	1
|_immer_mehr	1
immer_mehr_ausfälle	1
mehr_ausfälle_bei	1
den_sächsischen_kitas	1
sächsischen_kitas_fällt	1
kitas_fällt_pandemiebedingt	1
fällt_pandemiebedingt_immer	1
pandemiebedingt_immer_mehr	1
immer_mehr_personal	1
mehr_personal_aus.	1
personal_aus._gründe	1
aus._gründe_dafür	1
gründe_dafür_seien	1
dafür_seien_in	1
seien_in_der	1
in_der_regel	1
der_regel_quarantäne	1
regel_quarantäne_oder	1
quarantäne_oder_krankheit,	1
oder_krankheit,_wie	1
krankheit,_wie_die	1
wie_die_drei	1
die_drei_großstädte	1
drei_großstädte_dresden,	1
großstädte_dresden,_leipzig	1
dresden,_leipzig_und	1
leipzig_und_chemnitz	1
und_chemnitz_mitteilten.	1
chemnitz_mitteilten._oftmals	1
mitteilten._oftmals_müssten	1
oftmals_müssten_sich	1
müssten_sich_mitarbeiter	1
sich_mitarbeiter_auch	1
mitarbeiter_auch_um	1
auch_um_ihre	1
um_ihre_kranken	1
ihre_kranken_oder	1
kranken_oder_in	1
oder_in_quarantäne	1
in_quarantäne_befindenden	1
quarantäne_befindenden_kinder	1
befindenden_kinder_kümmern	1
kinder_kümmern_und	1
kümmern_und_fielen	1
und_fielen_deshalb	1
fielen_deshalb_aus.	1
deshalb_aus._davon	1
aus._davon_berichtet	1
davon_berichtet_auch	1
berichtet_auch_die	1
auch_die_diakonie	1
die_diakonie_aus	1
diakonie_aus_ihren	1
aus_ihren_kitas.	1
ihren_kitas._eine	1
kitas._eine_nahezu	1
eine_nahezu_überall	1
nahezu_überall_auftretende	1
überall_auftretende_folge	1
auftretende_folge_der	1
folge_der_personalengpässe	1
der_personalengpässe_ist	1
personalengpässe_ist_die	1
ist_die_einschränkung	1
die_einschränkung_der	1
|_tausende_sachsen	1
tausende_sachsen_gegen	1
sachsen_gegen_corona-politik	1
gegen_corona-politik_auf	1
corona-politik_auf_der	1
sachsen_sind_am	1
sind_am_gestrigen	1
am_gestrigen_abend	1
gestrigen_abend_erneut	1
abend_erneut_tausende	1
erneut_tausende_menschen	1
tausende_menschen_gegen	1
menschen_gegen_die	1
gegen_die_corona-politik	1
die_corona-politik_auf	1
corona-politik_auf_die	1
auf_die_straße	1
die_straße_gegangen.	1
straße_gegangen._nach	1
gegangen._nach_bisherigen	1
nach_bisherigen_angaben	1
bisherigen_angaben_verliefen	1
angaben_verliefen_die	1
verliefen_die_versammlungen	1
die_versammlungen_in	1
versammlungen_in_verschiedenen	1
in_verschiedenen_städten	1
verschiedenen_städten_weitgehend	1
städten_weitgehend_störungsfrei.	1
weitgehend_störungsfrei._in	1
störungsfrei._in_bautzen	1
in_bautzen_kamen	1
bautzen_kamen_rund	1
kamen_rund_3.000	1
rund_3.000_menschen	1
3.000_menschen_zum	1
menschen_zum_protest	1
zum_protest_gegen	1
protest_gegen_die	1
gegen_die_corona-maßnahmen	1
die_corona-maßnahmen_zusammen.	1
corona-maßnahmen_zusammen._im	1
zusammen._im_gesamten	1
im_gesamten_landkreis	1
gesamten_landkreis_sowie	1
landkreis_sowie_im	1
sowie_im_landkreis	1
im_landkreis_görlitz	1
landkreis_görlitz_haben	1
görlitz_haben_in	1
haben_in_der	1
in_der_summe	1
der_summe_bis	1
summe_bis_zu	1
bis_zu_12.700	1
zu_12.700_menschen	1
12.700_menschen_an	1
menschen_an_angemeldeten	1
an_angemeldeten_und	1
angemeldeten_und_nicht	1
und_nicht_angemeldeten	1
nicht_angemeldeten_versammlungen	1
angemeldeten_versammlungen_teilgenommen,	1
versammlungen_teilgenommen,_wie	1
teilgenommen,_wie_die	1
wie_die_polizei	1
die_polizei_mitteilte.	1
polizei_mitteilte._in	1
mitteilte._in_dresden	1
in_dresden_schlossen	1
dresden_schlossen_sich	1
schlossen_sich_mehrere	1
sich_mehrere_hundert	1
mehrere_hundert_menschen	1
hundert_menschen_einem	1
menschen_einem_corona-aufzug	1
einem_corona-aufzug_an.	1
corona-aufzug_an._ähnliche	1
an._ähnliche_proteste	1
ähnliche_proteste_wurden	1
proteste_wurden_aus	1
wurden_aus_freiberg,	1
aus_freiberg,_zwickau,	1
freiberg,_zwickau,_löbau	1
zwickau,_löbau_und	1
löbau_und_zittau	1
und_zittau_gemeldet.	1
zittau_gemeldet._in	1
gemeldet._in_pirna	1
in_pirna_wandten	1
pirna_wandten_sich	1
wandten_sich_pflegekräfte	1
sich_pflegekräfte_gegen	1
pflegekräfte_gegen_die	1
gegen_die_impfpflicht	1
die_impfpflicht_in	1
impfpflicht_in_ihrer	1
in_ihrer_branche.	1
ihrer_branche._die	1
branche._die_polizei	1
die_polizei_hat	1
polizei_hat_diverse	1
hat_diverse_ermittlungsverfahren	1
diverse_ermittlungsverfahren_im	1
ermittlungsverfahren_im_freistaat	1
im_freistaat_wegen	1
freistaat_wegen_der	1
wegen_der_verstöße	1
der_verstöße_gegen	1
|_eckpunkte_für	1
eckpunkte_für_neue	1
gegen_13_uhr	1
13_uhr_will	1
uhr_will_das	1
will_das_sächsische	1
das_sächsische_kabinett	1
sächsische_kabinett_eine	1
kabinett_eine_neue	1
neue_corona-notfallverordnung_beschließen.	1
corona-notfallverordnung_beschließen._die	1
beschließen._die_neue	1
die_neue_verordnung	1
neue_verordnung_soll	1
verordnung_soll_vom	1
soll_vom_6.februar	1
vom_6.februar_bis	1
6.februar_bis_6.märz	1
bis_6.märz_gelten.	1
6.märz_gelten._schon	1
gelten._schon_bei	1
schon_bei_der	1
bei_der_vorstellung	1
der_vorstellung_der	1
vorstellung_der_eckpunkte	1
der_eckpunkte_hatte	1
eckpunkte_hatte_sozialheitsministerin	1
hatte_sozialheitsministerin_petra	1
sozialheitsministerin_petra_köpping	1
köpping_(spd)_lockerungen	1
(spd)_lockerungen_in	1
lockerungen_in_aussicht	1
in_aussicht_gestellt.	1
aussicht_gestellt._so	1
gestellt._so_sollen	1
so_sollen_künftigmessen	1
sollen_künftigmessen_und	1
künftigmessen_und_kongresse	1
und_kongresse_wieder	1
kongresse_wieder_unter	1
wieder_unter_2g	1
unter_2g_(genesen	1
2g_(genesen_oder	1
(genesen_oder_geimpft)	1
oder_geimpft)_und	1
geimpft)_und_einer	1
und_einer_kapazitätsbegrenzung	1
einer_kapazitätsbegrenzung_möglich	1
kapazitätsbegrenzung_möglich_sein.	1
möglich_sein._bei	1
sein._bei_eheschließungen	1
bei_eheschließungen_und	1
eheschließungen_und_beerdigungen	1
und_beerdigungen_mehr	1
beerdigungen_mehr_personen	1
mehr_personen_teilnehmen	1
personen_teilnehmen_können	1
teilnehmen_können_als	1
können_als_bisher.	1
als_bisher._in	1
bisher._in_sachsen	1
in_sachsen_wieder	1
sachsen_wieder_mehr	1
wieder_mehr_zuschauer	1
zuschauer_in_die	1
in_die_stadien	1
die_stadien_kommen	1
stadien_kommen_und	1
kommen_und_nicht	1
und_nicht_wie	1
nicht_wie_bislang	1
wie_bislang_nur	1
bislang_nur_1.000.	1
nur_1.000._die	1
1.000._die_hotspot-regel	1
die_hotspot-regel_soll	1
hotspot-regel_soll_doch	1
soll_doch_nicht	1
|_debatte_um	1
robert-koch-institut_begrenzt_den	1
begrenzt_den_genesenen-status	1
den_genesenen-status_für	1
genesenen-status_für_ungeimpfte	1
für_ungeimpfte_auf	1
ungeimpfte_auf_drei	1
auf_drei_monate,	1
drei_monate,_um	1
monate,_um_druck	1
um_druck_auf	1
druck_auf_eine	1
auf_eine_frühe	1
eine_frühe_booster-impfung	1
frühe_booster-impfung_zu	1
booster-impfung_zu_machen.	1
zu_machen._davon	1
machen._davon_profitieren	1
davon_profitieren_wohl	1
profitieren_wohl_nicht	1
wohl_nicht_alle	1
nicht_alle_genesenen	1
alle_genesenen_und	1
genesenen_und_auch	1
und_auch_der	1
auch_der_nutzen	1
der_nutzen_für	1
nutzen_für_die	1
für_die_pandemie-bekämpfung	1
die_pandemie-bekämpfung_ist	1
pandemie-bekämpfung_ist_fraglich.	1
ist_fraglich._forschende	1
fraglich._forschende_haben	1
forschende_haben_unterschiedliche	1
haben_unterschiedliche_kritik	1
unterschiedliche_kritik_an	1
kritik_an_der	1
an_der_begrenzung.	1
der_begrenzung._die	1
begrenzung._die_debatte	1
die_debatte_in	1
debatte_in_der	1
