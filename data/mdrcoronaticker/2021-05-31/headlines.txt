###21:20 | tickerende
###20:48 | tv-tipp: sehenswerte doku zu corona-impfungen
###19:55 | sachsen und tschechien vertiefen grenzüberschreitenden austausch
###19:02 | landessportbund verärgert über neue testregeln
###18:25 | zahl der aktiven corona-infektionsfälle sinkt weiter
###17:32 | eu-kommission lässt biontech-impfstoff für kinder zu
###16:53 | corona-lockerungen: was ist ab heute möglich.
###15:49 | nordsachsen kündigt weitere lockerungen an
###14:31 | kein märchen: fabulix -festival abgesagt
###13:21 | philharmonie spielt wieder vor publikum im kulturpalast dresden
###13:07 | zoos und tierparks dürfen innenanlagen öffnen
###12:57 | impftermine für über 14-jährige in sachsen ab dieser woche
###12:46 | lehrergewerkschaft erleichtert über kita- und schulöffnungen
###11:36 | hinweisschilder in bautzen werden abgenommen - dresden wartet
###10:26 | verlosung für künstlerurlaube in seifhennersdorf
###09:51 | über 500 freie termine für impfaktion in flöha
###09:02 | personalmangel in leipzigs gastrobetrieben
###08:30 | info-wochen der tu dresden digital
###08:09 | stadtverwaltung bautzen öffnet wieder
###07:51 | weitere lockerungen in leipzig
###07:17 | sachsenweite inzidenz steigt leicht
###06:57 | breslau wartet auf touristen
###06:32 | regelbetrieb in grundschulen in sächsischer schweiz
###06:20 | erneut übergriff auf polizei bei protest in zwönitz
###05:49 | haben die zusätzlichen impfdosen im erzgebirge geholfen.
###05:23 | gesundheitsministerin köpping: nicht übermütig werden
###05:00 | sachsen lockert einschränkungen für handel, freizeitparks und freibäder
