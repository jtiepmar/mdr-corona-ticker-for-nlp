import re
import os

path = "data/" # Ordner mit den Tagesordnern
list_of_files = []


##### Get Filenames #####
for (dirpath, dirnames, filenames) in os.walk(path):
	for filename in filenames:
		if filename.endswith('.txt') and not filename.endswith('_clean.txt'): 
			list_of_files.append(str(os.path.join(dirpath, filename)))

##### Text-Preprocessing #####
for i in list_of_files:
	print(i)
	
	if i[16:-4] == "bagofwords":
		continue
	
	f = open(i, "r", encoding="utf8")
		
	textList = ""
	
	for line in f:
		f2 = re.sub(r"\{.*?\}", " ", line)
		f2 = re.sub(r"\@(.*?)\}", " ", f2)
		f2 = re.sub(r"([a-z]+)([A-Z])", r"\1 \2", f2)
		f2 = re.sub(r"([a-z]+\.)([A-Z])", r"\1 \2", f2)
		f2 = re.sub(r"https\:(.*)\.html", r" ", f2)
		f2 = f2.replace("..container", "")
		f2 = f2.replace("..", ".")
		f2 = re.sub(r"\<.*?\>", " ", f2)
		textList += f2
	
	f.close()
	
	new = i[:-4]
	new += "_clean.txt"
	
	f = open(new, "w", encoding="utf8")
	f.write(textList)
	f.close()


