from bertopic import BERTopic

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.utils import Bunch

import re

import os

from stopwords import stopwords


def doc_loader(f):

    out = []
    tmp = ""
    counter = 0
    for line in f:
        if "|" in line:
            tmpSplit = line.split("|")
            #time = re.findall("/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/", tmpSplit[0])
            regex = '([0-9]{1,2}):([0-9]{1,2})'
            time = re.findall(regex, tmpSplit[0])
            tmp = tmpSplit[1]
            tmp += " "
            counter += 1
        else:
            if counter == 0:
                continue
            tmp += line
            tmp += " "
            while "  " in tmp:
                tmp = tmp.replace("  ", " ")
            try:
                out.append([time[0], tmp])
            except IndexError:
                out.append([tmpSplit[0].replace("#", "").replace(" ", ""), tmp])
            tmp = ""
            counter = 0
    
    return out




path = "data"
docs = []
list_of_files = []


for (dirpath, dirnames, filenames) in os.walk(path):
    for filename in filenames:
        if filename.endswith('normalized.txt'): 
            list_of_files.append(str(os.path.join(dirpath, filename)))


times = []
dates = []
news = []

for file in list_of_files:
    f = open(file, "r", encoding="utf8")
    fRead = f.read().split("\n")
    f.close()
    tmp = doc_loader(fRead)
    for i in tmp:
        uhr = str(i[0]).replace("Uhr", "").replace(",", ":").replace("'", "").replace("(", "").replace(")", "").replace(" ", "")
        if uhr.isnumeric():
                uhr = uhr[:2] + ":" + uhr[2:4]
        times.append(uhr)


        dates.append(file[11:-15]) # <-- needs to be adjusted to folder structure
        try:
            news.append(i[1])
        except IndexError:
            print(tmp)

    
    
docs = Bunch(time=times, date=dates, news=news)

vectorizer_model = CountVectorizer(ngram_range=(1, 2), stop_words=stopwords)

bert_model = "distiluse-base-multilingual-cased-v1"

topic_model = BERTopic(embedding_model=bert_model,
                       vectorizer_model=vectorizer_model,
                       calculate_probabilities=True,
                       verbose=True,
                       nr_topics=30,
                       top_n_words=30,
                       min_topic_size=15,
                       n_gram_range=(1, 2))

topics, probs = topic_model.fit_transform(docs.news)


f = open("topics_docs.txt", "w", encoding="utf8")
f.write("Datum\tUhrzeit\tTopic\n")

for i in range(len(topics)):
    outTmp = str(docs.date[i]) + "\t" + str(docs.time[i]) + "\t" + str(topics[i]) + "\n"
    f.write(outTmp)
    
f.close()


out = ""
out += "Topic: " + str(-1) + "\n"
out += str(topic_model.get_topic(-1))
out += "\n\n"

for i in range(len(topic_model.get_topics())):
    out += "Topic: " + str(i) + "\n"
    out += str(topic_model.get_topic(i))
    out += "\n\n"
f = open("topics_out_bert_day.txt", "w", encoding="utf8")
f.write(out)
f.close()




