import os
import re

import random


workingf = "data/"
alltexts = ""
allheadlines=""
alltextsandheadlines = ""
alltypetokencounts = "text\ttypes\ttokens\ttypes/tokens\n"

if not os.path.exists(workingf+"_all"):
	os.mkdir(workingf+"_all")




def typetokenratio(text):
	dc = {}
	arr = text.split(" ")
	for type in arr:
		if len(type)>0 and not type in dc:
			dc[type] = 1

	if len(dc.keys()) > 0:
		return len(arr)/len(dc.keys())
	else:
		return 0
	
def tokencount(text):
	text = text.strip()
	if len(text)>0:
		tokens = len(text.split(" "))+1
	else:
		tokens=0
	return tokens

def typecount(text):
	dc = {}
	arr = text.split(" ")
	for wordtype in arr:
		if len(wordtype)>0 and not wordtype in dc:
			dc[wordtype] = 1
	return len(dc.keys())

def sentencecollocations(text):
	sentencelist=[]
	sentences= text.split(". ")
#	random.shuffle(sentences)
	for sent in sentences:
		dc = {}
		arr = sent.split(" ")
		for type in arr:
			if len(type)>0:
				if type in dc:
					dc[type] = dc[type]+1
				else:
					dc[type] = 1
		l = list(dc.items())
		#random.shuffle(l)
		sentencelist.append(dict(l))
		
	return sentencelist
	
def bagofwords(text):
	dc = {}
	arr = text.split(" ")
	for type in arr:
		if len(type)>0:
			if type in dc:
				dc[type] = dc[type]+1
			else:
				dc[type] = 1
	l = list(dc.items())
	l.sort(key=lambda x: x[1], reverse=True)
	return dict(l)
	
def ngrams(text,size):
	dc = {}
	if("\t" in line):
		print(line)
		exit()
	arr = text.split(" ")
	i = 1
	while(i<len(arr)):
		ngram = ""
		n = 0
		while(n<=size and i+n<len(arr)):
			ngram = ngram+" "+arr[i+n]
			n=n+1
			if n==size:
				ngram=ngram.strip().replace(" ","_")
				if not "\n" in ngram :
					if ngram in dc:
						dc[ngram] = dc[ngram]+1
					else:
						dc[ngram] = 1
		i = i+1
	l = list(dc.items())
	l.sort(key=lambda x: x[1], reverse=True)
	return dict(l)
	
def bagofwords(text):
	dc = {}
	text=text.lower().replace("\n"," ").replace("."," ").replace(","," ")
	text = re.sub(r'[^\w\s]',' ',text)

	whitelist = set('abcdefghijklmnopqrstuvwxyzäöüß ')
	text = ''.join(filter(whitelist.__contains__, text))
	while("  " in text):
		text = text.replace("  "," ")
	arr = text.split(" ")
	for type in arr:
		if len(type)>0:
			if type in dc:
				dc[type] = dc[type]+1
			else:
				dc[type] = 1
	l = list(dc.items())
	l.sort(key=lambda x: x[1], reverse=True)
	return dict(l)
	
def dictToLinetab(dicti):
	dictitext = "" 
	for key in dicti.keys():
		dictitext+=str(key)+"\t"+str(dicti[key])+"\n"
	return dictitext
def dictToNewline(text):
	return text.replace("}, {","\n").replace("'","").replace("[","").replace("]","").replace("{","").replace("}","").replace(" ","")

with open (workingf+"_all/dates.txt", "w") as f:
	for datef in os.listdir(workingf):
		if not "_" in datef:
			count=1
			for file in os.listdir(workingf+"/"+datef):
				if file.endswith("dupl"):
					count=count+1
			f.write(datef+"\t"+str(count)+"\n")

def cleanup(line):
	line = re.sub(r'<footer>(.*?)</footer>', ' ', line)
	line = re.sub('<[^<]+?>', ' ', line)
	line = re.sub(r"\{.*?\}", " ", line)
	line = re.sub(r"\@(.*?)\}", " ", line)
	line = re.sub(r"([a-z]+)([A-Z])", r"\1 \2", line)
	line = re.sub(r"([a-z]+\.)([A-Z])", r"\1 \2", line)
	line = re.sub(r"https\:(.*)\.html", r" ", line)
	line = line.replace("..container", " ").replace("/", " ").replace("\\", " ").replace(">", " ").replace("<", " ").replace('"', " ")
	line = line.replace("..", ".")
	line = line.replace("\t"," ")
	if not " | " in line:
		line = line.replace(":", " ")
	line = line.replace("?", ". ").replace(".", ". ").replace("!", ". ")
	line = re.sub(r"\<.*?\>", " ", line)
	while("  " in line):
		line = line.replace("  "," ")
	line = line.replace("Af D", "AfD")
	line = line.replace(". Januar", ".Januar")
	line = line.replace(". Februar", ".Februar")
	line = line.replace(". März", ".März")
	line = line.replace(". April", ".April")
	line = line.replace(". Mai", ".Mai")
	line = line.replace(". Juni", ".Juni")
	line = line.replace(". Juli", ".Juli")
	line = line.replace(". August", ".August")
	line = line.replace(". September", ".September")
	line = line.replace(". Oktober", ".Oktober")
	line = line.replace(". November", ".November")
	line = line.replace(". Dezember", ".Dezember")
	line = re.sub(r"(\d)\. (\d)", r"\1.\2", line)
	line = re.sub(r"(\D)\.(\D)", r"\1. \2", line)
	return line

	
for datef in os.listdir(workingf):
	if not datef.startswith("_"):
		with open(workingf+datef+"/index.html", "r", encoding="utf8") as file:
			text = ""
			ignore = True
			block = False
			for line in file:
				if line.strip().startswith("var ") or line.strip().startswith("Bildrechte:") or line.strip().startswith("Audio herunterladen") or line.strip()=="mehr" or line.strip()=="Neuer Abschnitt" or "Von Montag bis Sonntag halten wir Sie mit unserem Corona-Ticker über das Pandemiegeschehen in Sachsen auf dem Laufenden" in line:
					line = ""
				else:
					if "<script" in line or "<footer" in line or "<span" in line:
						block = True
						ignore = True

					if "</script" in line or "</footer" in line or "</span" in line:
						block = False
					line = cleanup(line)
					line = line.strip().replace("\n", " ")

					if "Uhr | " in line and not line.strip().endswith("MDR.DE"):
						ignore = False
						line = "\n\n###"+line+"\n"
					if line.strip() == "Neuer Abschnitt":
						ignore = True
					if line.strip() == "Neuer Bereich":
						ignore = True
					if not ignore and not block:
						text+=line
			while("  " in text):
				text = text.replace("  "," ")

			while("\n " in text):
				text = text.replace("\n ","\n")
			while("\n\n" in text):
				text = text.replace("\n\n","\n")
			
				
			text = text.split("Mehr zum Thema")[0].strip()
			#if len(text.split("###"))<10:
			#	print(datef)
			text = text.split("###MDR SACHSEN |")[0].strip()
			text = text.split("###MDR SACHSEN - Das Sachsenradio |")[0].strip()
			text = text.split("Ticker EndeTicker Ende")[0].strip()
			text = text.replace(" . ",". ")
			text = text.replace("1. FC","1.FC")
			text = text.lower().replace(" uhr | "," | ")
		with open(workingf+datef+"/normalized.txt", "w", encoding="utf8") as out:
			out.write(text)


		with open(workingf+datef+"/normalized.txt", "r", encoding="utf8") as file:
			headlines = ""
			texts = ""

			for line in file:
				if line.startswith("###"):
					headlines = headlines+line.strip()+"\n"
					alltextsandheadlines += line.split(" | ")[1].strip()+"\n"
					allheadlines += line.split(" | ")[1].strip()+"\n"
				else:
					texts = texts+line.strip()+"\n"
					alltextsandheadlines += line.strip()+"\n"
					alltexts += line.strip()+"\n"

#			alltextsandheadlines = alltextsandheadlines + headlines.strip().replace("###","")+" "+texts.strip()
#			alltexts = alltexts + " "+texts.strip()
#			allheadlines = allheadlines + " "+headlines.replace("###","").strip()
			with open(workingf+datef+"/headlines.txt", "w", encoding="utf8") as out:
				out.write(headlines)
			with open(workingf+datef+"/texts.txt", "w", encoding="utf8") as out:
				out.write(texts)
			with open(workingf+datef+"/bagofwords.txt", "w", encoding="utf8") as out:
				out.write(dictToLinetab(bagofwords(headlines+" "+texts)))

			with open(workingf+datef+"/texts_ngram3.txt", "w", encoding="utf8") as out:
				out.write(dictToLinetab(ngrams(text,3)))
			with open(workingf+datef+"/texts_ngram5.txt", "w", encoding="utf8") as out:
				out.write(dictToLinetab(ngrams(text,5)))
			with open(workingf+datef+"/texts_sentencecollocations.txt", "w", encoding="utf8") as out:
				out.write(dictToNewline(str(sentencecollocations(alltexts))))
		with open(workingf+datef+"/normalized.txt", "r", encoding="utf8") as file:
			tokenstats="text\ttypes\ttokens\ttypes/tokens\n"
			alltypetokencounts= alltypetokencounts+datef
			text = ""
			
			for line in file:
				if line.startswith("###"):
					tokenstats = tokenstats+line.split("|")[0].replace("###","").strip()
				else:
					text = text+" "+line
					tokenstats = tokenstats+"\t"+ str(typecount(line))+"\t"+ str(tokencount(line))+"\t"+ str(round(typetokenratio(line),4))+"\n"
				text = text.strip()
			alltypetokencounts = alltypetokencounts+"\t"+ str(typecount(text))+"\t"+ str(tokencount(text))+"\t"+ str(round(typetokenratio(text),4))+"\n"
			tokenstats = tokenstats.replace("uhr2","uhr\t\t\t\n2")
			with open(workingf+datef+"/typetokencount.txt", "w", encoding="utf8") as out:
				out.write(tokenstats)
			print(datef)


with open(workingf+"_all/bagofwords_allheadlines.txt", "w", encoding="utf8") as out:
	out.write(dictToLinetab(bagofwords(allheadlines)))
with open(workingf+"_all/bagofwords_alltexts.txt", "w", encoding="utf8") as out:
	out.write(dictToLinetab(bagofwords(alltexts)))
with open(workingf+"_all/bagofwords_alltextsandheadlines.txt", "w", encoding="utf8") as out:
	out.write(dictToLinetab(bagofwords(alltextsandheadlines)))


with open(workingf+"_all/alltexts_ngram3.txt", "w", encoding="utf8") as out:
	out.write(dictToLinetab(ngrams(alltexts,3)))
with open(workingf+"_all/alltexts_ngram5.txt", "w", encoding="utf8") as out:
	out.write(dictToLinetab(ngrams(alltexts,5)))
with open(workingf+"_all/alltexts_sentencecollocations.txt", "w", encoding="utf8") as out:
	out.write(dictToNewline(str(sentencecollocations(alltexts))))

with open(workingf+"_all/alltextsandheadlines.txt", "w", encoding="utf8") as out:
	out.write(alltextsandheadlines)
with open(workingf+"_all/alltexts.txt", "w", encoding="utf8") as out:
	out.write(alltexts)
with open(workingf+"_all/allheadlines.txt", "w", encoding="utf8") as out:
	out.write(allheadlines)


with open(workingf+"_all/typetokencount.txt", "w", encoding="utf8") as out:
	out.write(alltypetokencounts)


alltypes = {}
datetypes = {}
for datef in os.listdir(workingf):
	if not datef.startswith("_"):
		with open(workingf+datef+"/bagofwords.txt", "r", encoding="utf8") as file:
			newTypes=""
			text = ""
			for line in file:
				wordtype = line.split("\t")[0]
				if len(wordtype)>0 and not wordtype in alltypes:
					alltypes[wordtype] = 1
					newTypes += wordtype+","
			datetypes[datef] = str(len(alltypes.keys()))+"\t"+newTypes[:-1]
with open(workingf+"_all/datetypecount.txt", "w", encoding="utf8") as file:
	file.write("datum\ttypecount\n")
	for date in datetypes:
		file.write(date+"\t"+datetypes[date]+"\n")
		