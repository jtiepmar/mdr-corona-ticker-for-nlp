import requests
import urllib.request
import time
from bs4 import BeautifulSoup
import os
import shutil
import re
import locale
from datetime import datetime

if not os.path.exists("data"):
	os.mkdir("data")
locale.setlocale(locale.LC_ALL, 'de_DE.utf8')

link_file = "links-ticker-mdr.txt"

# checkt, ob Liste mit Links bereits vorhanden
def check_list_exists(name):
	if os.path.exists(name):
		with open(name, "r") as file:
			links = [link.rstrip() for link in file.readlines()]
	else:
		links = []
	return links

links = check_list_exists(link_file)

new_links = [] # Liste fuer noch nicht gespeicherte Links

url = "https://www.mdr.de/nachrichten/sachsen/corona-ticker-sachsen-archiv-100.html"
response = requests.get(url)
soup = BeautifulSoup(response.text, "html.parser")
# gleicht gespeicherte mit aktuellen Links ab und speichert diese in new_links
for s in soup.findAll(class_="linkAll", href=True):
	link = s["href"]
	if (link not in links):
		new_links.append(link)

# ruft jeden new_link auf und speichert die html-Datei
for nl in new_links:
	url = 'https://www.mdr.de' + nl
	response = requests.get(url)
	soup = BeautifulSoup(response.text, 'html.parser')
	dtime = soup.find(class_="webtime").get_text()
	dtime = re.sub("\n", "", dtime)
	print(dtime)
	dtime = datetime.strptime(dtime, "Stand: %d. %B %Y, %H:%M Uhr")
	date = str(dtime.date())
	dtime = str(dtime).replace(":","_")
	#print(soup)
#	soup=soup.decode('utf-8','ignore').encode("utf-8")
#	text = text.decode('utf-8', 'ignore')
	filename = "index.html"
	if (not os.path.exists("data/"+date)):
		os.mkdir("data/"+date)
	while os.path.exists("data/"+date+"/"+filename):
		filename = filename+"_dupl"
	with open("data/"+date+"/"+filename, "w", encoding="utf-8") as file:
		try:
			file.write('<h1>Hinweise</h1><p>Dies ist ein lokales Backup als Forschungsresource. Dynamische Inhalte, Video und Audio sind nicht enthalten. Die originale Seite ist <a href="'+url+'"+>hier verlinkt<a>.<br>Gegebenenfalls eingebundene Bilddateien werden vom Browser dynamisch von der Quelle geladen (Rechtsklick->Bildaddresse); zur <b>Wahrung der Urheberrechte</b> wurden solche Inhalte nicht kopiert. <br><br><p>'+str(soup))
		except:
			print(nl+" Fehlerhaft")
		links.append(nl) # neuer Link wird der Liste aller Links hinuzgefuegt
	time.sleep(1)

# Liste aller Links wird ueberschrieben
with open("links-ticker-mdr.txt", "w") as file:
	for l in links:
		file.writelines('%s\n' % l)
