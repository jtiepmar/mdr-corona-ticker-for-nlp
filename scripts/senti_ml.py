from transformers import pipeline
import re
import os


def doc_loader(f):

	out = []
	tmp = ""
	counter = 0
	for line in f:
		if not "daisho" in line.lower():
			if "|" in line:
				tmpSplit = line.split("|")
				regex = '[0-9]{1,2}:[0-9]{1,2}'
				time = re.findall(regex, tmpSplit[0])
				tmp = tmpSplit[1]
				tmp += " "
				counter += 1
			else:
				if counter == 0:
					continue
				tmp += line
				tmp += " "
				while "  " in tmp:
					tmp = tmp.replace("  ", " ")
				try:
					out.append([time[0], tmp])
				except IndexError:
					out.append([tmpSplit[0].replace("#", "").replace(" ", ""), tmp])
				tmp = ""
				counter = 0

	return out

classifier = pipeline("sentiment-analysis", model="oliverguhr/german-sentiment-bert")


path = "data"
list_of_files = []


for (dirpath, dirnames, filenames) in os.walk(path):
	for filename in filenames:
		if filename.endswith('normalized.txt'): 
			list_of_files.append(str(os.path.join(dirpath, filename)))



wholeFile = "datum\tuhrzeit\tsentiment\tscore\ttext\n"

allFiles = len(list_of_files)
counter=0

for filedir in list_of_files:
	counter = counter+1
	f = open(filedir, "r", encoding="utf8")

	for i in doc_loader(f):
		senti = classifier(i[1])
		tmpSenti = str(i[0]).replace("uhr", "").replace(" ", "") + "\t" + str(senti[0]['label']).replace(" ", "") + "\t" + str(senti[0]['score']).replace(" ", "") + '\t' + i[1].replace("\n", " ") + '\n'
		wholeFile += filedir[5:-15] + "\t" + tmpSenti

	f.close()
	print("Senti_ML "+str(counter)+"/"+str(allFiles))

f = open("data/_all/senti_ml.txt", "w", encoding="utf8")
f.write(wholeFile)
f.close()