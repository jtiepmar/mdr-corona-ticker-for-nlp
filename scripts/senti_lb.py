##### Citation: #######
#
#
# http://asv.informatik.uni-leipzig.de/publication/file/155/490_Paper.pdf
#
# https://github.com/Liebeck/spacy-sentiws
#
#
#############

import os
import spacy
from spacy_sentiws import spaCySentiWS
from stopwords import *
import re

def doc_loader(f):
	out = []
	tmp = ""
	counter = 0
	for line in f:
		if "|" in line:
			tmpSplit = line.split("|")
			regex = '[0-9]{1,2}:[0-9]{1,2}'
			time = re.findall(regex, tmpSplit[0])
			tmp = tmpSplit[1]
			tmp += " "
			counter += 1
		else:
			if counter == 0:
				continue
			tmp += line
			tmp += " "
			while "  " in tmp:
				tmp = tmp.replace("  ", " ")
			try:
				out.append([time[0], tmp])
			except IndexError:
				out.append([tmpSplit[0].replace("#", "").replace(" ", ""), tmp])
			tmp = ""
			counter = 0
	return out

def senti(list_of_files):
	nlp = spacy.load('de_core_news_lg')
	nlp.add_pipe('sentiws', config={'sentiws_path': 'sentiws'})
	counter=0
	wholeFile = "datum\tuhrzeit\tsentiment\tscore\ttext\n"
	for filedir in list_of_files:
		counter += 1
		f = open(filedir, "r", encoding="utf8")
		for i in doc_loader(f):
			sentiPos = 0
			sentiNeg = 0
			doc = nlp(i[1].lower())
			counterPos = 0
			counterNeg = 0
			for token in doc:
				if token.text in stopwords:
					continue
				elif token.lemma_ in stopwords:
					continue
				elif token._.sentiws != None:
					if token._.sentiws < 0:
						counterNeg += 1
						sentiNeg += token._.sentiws
					elif token._.sentiws > 0:
						counterPos += 1
						sentiPos += token._.sentiws
			try:		
				total = (sentiPos + sentiNeg) / (counterPos + counterNeg)
			except ZeroDivisionError:
				total = 0
			try:
				avgPos = sentiPos / counterPos
			except ZeroDivisionError:
				avgPos = 0
			try:
				avgNeg = sentiNeg / counterNeg
			except ZeroDivisionError:
				avgNeg = 0
			uhr = str(i[0]).replace("uhr", "").replace(" ", "")
			if uhr.isnumeric():
				uhr = uhr[:2] + ":" + uhr[2:4]
			tmpSenti = uhr + "\t"
			if round(total,4) <=-0.2:
				tmpSenti+="negative\t"
			if round(total,4) >=0.2:
				tmpSenti+="positive\t"
			if round(total,4) <0.2 and round(total,4) >-0.2:
				tmpSenti+="neutral\t"

			tmpSenti += str(round(total,4)) + "\t"
#			tmpSenti += str(round(sentiPos,4)) + "\t" + str(round(sentiNeg,4)) + "\t"
#			tmpSenti += str(round(avgPos,4)) + "\t" + str(round(avgNeg,4)) + "\t"
			tmpSenti += i[1].replace("\n", " -").strip() + '\n'
			wholeFile += filedir[5:-15] + "\t" + tmpSenti
		print("SentiWS "+str(counter)+"/"+str(len(list_of_files)))
	f.close()
	return wholeFile

path = "data"
list_of_files = []
for (dirpath, dirnames, filenames) in os.walk(path):
	for filename in filenames:
		if filename.endswith('normalized.txt'): 
			list_of_files.append(str(os.path.join(dirpath, filename)))
wholeFile = senti(list_of_files)
f = open("data/_all/senti_lb.txt", "w", encoding="utf8")
f.write(wholeFile)
f.close()