import os
if not os.path.exists("data/_tm"):
	os.mkdir("data/_tm")
if not os.path.exists("data/_tm/topdocs"):
	os.mkdir("data/_tm/topdocs")
topdoc = {}

with open ("topics_docs.txt", "r", encoding="utf8") as f:
	next(f)
	for line in f:
		arr = line.split("\t")
		arr[2] = arr[2].strip()
		msg = ""
		with open ("data/"+arr[0]+"/normalized.txt", encoding="utf8") as msgf:
			for msgline in msgf:
				if msgline.startswith("###"):
					msgarrtime = msgline.split(" | ")[0].replace("###","").strip()
					if msgarrtime == arr[1]:
						msg = next(msgf).strip()
		if arr[2] in topdoc:
			topdoc[arr[2]] += arr[0]+"\t"+arr[1]+"\t"+msg+"\n"
		else:
			topdoc[arr[2]] = arr[0]+"\t"+arr[1]+"\t"+msg+"\n"

for top in topdoc:
	with open("data/_tm/topdocs/"+top+".txt", "w", encoding="utf8") as f:
		f.write("Datum\tUhrzeit\tNachricht\n"+topdoc[top])
		
#print(topdoc)

text = ""
text2 = "Topic\tTerms\n"
text3 = "Topic\tTerms\n"
with open ("topics_out_bert.txt", "r", encoding="utf8") as f:
	for line in f:
		text += line
	text = text.replace(")]","").replace("', ",":").replace("\r\n","\n").replace("\n[(","\t").replace("), (",",").replace("'","").replace("Topic: ","").replace("\n\n","\n").replace("\nFalse","#False")
for line in text.split("\n"):
	if not "#False" in line:
		text2 += line+"\n"
		arr = line.split(",")
		for term in arr:
			term = term.split(":")[0]+","
			text3 += term
		text3 +="\n"
while ",\n" in text3:
	text3 = text3.replace(",\n","\n")

with open("data/_tm/topics_probs.txt", "w", encoding="utf8") as f:
	f.write(text2.strip())
	
with open("data/_tm/topics.txt", "w", encoding="utf8") as f:
	f.write(text3.strip())


#with open("data/_tm/topics_probs.txt", "w", encoding="utf8") as f:
#	f.write(text2.strip())
	
#with open("data/_tm/topics.txt", "w", encoding="utf8") as f:
#	f.write(text3.strip())

