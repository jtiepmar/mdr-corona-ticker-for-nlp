import re
import os

path = "data/" # Ordner mit den Tagesordnern
list_of_files = []
file_str = {}

##### Get Filenames #####
for (dirpath, dirnames, filenames) in os.walk(path):
	for filename in filenames:
		if filename.endswith('Flair.txt'):# and "2021-05-09" in dirpath: 
			list_of_files.append(str(os.path.join(dirpath, filename)))

if not os.path.exists(path+"_ner"):
	os.mkdir(path+"_ner")
if not os.path.exists(path+"_ner/person_Flair"):
	os.mkdir(path+"_ner/person_Flair")
if not os.path.exists(path+"_ner/ort_Flair"):
	os.mkdir(path+"_ner/ort_Flair")
if not os.path.exists(path+"_ner/organisation_Flair"):
	os.mkdir(path+"_ner/organisation_Flair")
if not os.path.exists(path+"_ner/sonstiges_Flair"):
	os.mkdir(path+"_ner/sonstiges_Flair")


##### Text-Preprocessing #####
for i in list_of_files:
	#print(i)
	
	f = open(i, "r", encoding="utf8")
	
	textList = ""
	date = i.split("\\normalized_")[0].replace(path,"")
	nertype = i.split("\\normalized_")[1].replace(".txt","")
	#print(date)
	for line in f:
		arr = line.split("\t")
		fn = path+"_ner/"+nertype+"/"+arr[1].replace(":","_").replace("!","_").replace('"',"_").replace('/',"_").replace('?',"_")+".txt"
		if not fn in file_str:
			file_str[fn] = date +"\t" + line
		else:
			file_str[fn] = file_str[fn]  + date +"\t" + line
	
		
	f.close()
	
for key in file_str:
	f = open(key, "w", encoding="utf8")
	f.write(file_str[key])
	f.close()
	print(key)

for folder in  os.listdir(path+"/_ner"):
	with open (path+"_all/"+folder+".txt", "w", encoding="utf8") as allout:
		for file in os.listdir(path+"_ner/"+folder):
			with open (path+"_ner/"+folder+"/"+file, "r", encoding="utf-8") as allin:
				for line in allin:
					allout.write(line)
			print(file)