import os

statfile = open("data/_all/stats.txt","w",encoding="utf8")
s="\t"
n = "\n"

with open("data/_all/dates.txt","r",encoding="utf8") as f:
	counter = 0
	for line in f:
		counter+=1
	statfile.write("days"+s+str(counter)+n)

with open("data/_all/allheadlines.txt","r",encoding="utf8") as f:
	counter = 0
	length=0
	mini = 1000000
	maxi = 0
	for line in f:
		counter+=1
		length+=len(line.strip())
		if(len(line.strip())<mini):
			mini = len(line.strip())
		if(len(line.strip())>maxi):
			maxi = len(line.strip())
	statfile.write("news"+s+str(counter)+n)
	statfile.write("sum characters headlines"+s+str(length)+n)
	statfile.write("avg characters headlines"+s+str(round(length/counter,4))+n)
	statfile.write("min characters headlines"+s+str(mini)+n)
	statfile.write("max characters headlines"+s+str(maxi)+n)

with open("data/_all/alltexts.txt","r",encoding="utf8") as f:
	counter = 0
	length=0
	mini = 1000000
	maxi = 0
	for line in f:
		counter+=1
		length+=len(line.strip())
		if(len(line.strip())<mini):
			mini = len(line.strip())
		if(len(line.strip())>maxi):
			maxi = len(line.strip())
	statfile.write("sum characters texts"+s+str(length)+n)
	statfile.write("avg characters texts"+s+str(round(length/counter,4))+n)
	statfile.write("min characters texts"+s+str(mini)+n)
	statfile.write("max characters texts"+s+str(maxi)+n)
	
with open("data/_all/bagofwords_alltextsandheadlines.txt","r",encoding="utf8") as f:
	types = 0
	tokens = 0
	for line in f:
		arr = line.split(s)
		tokens += int(arr[1])
		types += 1
	statfile.write("token count"+s+str(tokens)+n)
	statfile.write("type count"+s+str(types)+n)

with open("data/_all/bagofwords_allheadlines.txt","r",encoding="utf8") as f:
	types = 0
	tokens = 0
	for line in f:
		arr = line.split(s)
		tokens += int(arr[1])
		types += 1
	statfile.write("token count headlines"+s+str(tokens)+n)
	statfile.write("type count headlines"+s+str(types)+n)



with open("data/_all/bagofwords_alltexts.txt","r",encoding="utf8") as f:
	types = 0
	tokens = 0
	for line in f:
		arr = line.split(s)
		tokens += int(arr[1])
		types += 1
	statfile.write("token count texts"+s+str(tokens)+n)
	statfile.write("type count texts"+s+str(types)+n)

with open("data/_all/organisation_Flair.txt","r",encoding="utf8") as f:
	counter = 0
	for line in f:
		counter+=1
	statfile.write("NER organisations"+s+str(counter)+n)

with open("data/_all/person_Flair.txt","r",encoding="utf8") as f:
	counter = 0
	for line in f:
		counter+=1
	statfile.write("NER persons"+s+str(counter)+n)

with open("data/_all/ort_Flair.txt","r",encoding="utf8") as f:
	counter = 0
	for line in f:
		counter+=1
	statfile.write("NER places"+s+str(counter)+n)

with open("data/_all/sonstiges_Flair.txt","r",encoding="utf8") as f:
	counter = 0
	for line in f:
		counter+=1
	statfile.write("NER other"+s+str(counter)+n)

with open("data/_all/alltexts_ngram3.txt","r",encoding="utf8") as f:
	types = 0
	tokens = 0
	for line in f:
		arr = line.split(s)
		tokens += int(arr[1])
		types += 1
	statfile.write("ngrams3"+s+str(types)+n)
	statfile.write("unique ngrams3"+s+str(tokens)+n)
with open("data/_all/alltexts_ngram5.txt","r",encoding="utf8") as f:
	types = 0
	tokens = 0
	for line in f:
		arr = line.split(s)
		tokens += int(arr[1])
		types += 1
	statfile.write("ngrams5"+s+str(types)+n)
	statfile.write("unique ngrams5"+s+str(tokens)+n)
statfile.flush()
statfile.close()
