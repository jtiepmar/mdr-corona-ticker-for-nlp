
#####
# flair is normally thought to be used with sentences.
# I tested it and putting the whole document into the predicter worked fine as well.
# I decided for that option, because it gave the correct position of the NE in the document.
######

#%pip install flair


from flair.data import Sentence
from flair.models import SequenceTagger
from flair.tokenization import SegtokSentenceSplitter

import re
import os

done = ""
if os.path.exists("NER_done.txt"):
	with open("NER_done.txt", "r", encoding="utf8") as f:
		for line in f:
			done +=line 

# https://huggingface.co/flair/ner-german-large
tagger = SequenceTagger.load("flair/ner-german-large")

path = "data/" # Ordner mit den Tagesordnern
list_of_files = []


##### Get Filenames #####
for (dirpath, dirnames, filenames) in os.walk(path):
	for filename in filenames:
		if filename.endswith('normalized.txt') and not 'bagofwords' in filename and not '_all' in dirpath and not '_Flair' in filename: 
			if not str(os.path.join(dirpath, filename)) in done:
				list_of_files.append(str(os.path.join(dirpath, filename)))




##### NER #####
for i in list_of_files:
	print(i)

	f = open(i, "r", encoding="utf8")
	
	sentence = Sentence(str(f.read()))
	tagger.predict(sentence)
	
	outPerson = i[:16]+ i[16:-4] + "_person_Flair.txt"
	outOrt = i[:16] + i[16:-4] + "_ort_Flair.txt"
	outOrga = i[:16] + i[16:-4] + "_organisation_Flair.txt"
	outSonst = i[:16] + i[16:-4] + "_sonstiges_Flair.txt"
	
	outPersonF = open(outPerson, "w", encoding="utf8")
	outOrtF = open(outOrt, "w", encoding="utf8")
	outOrgaF = open(outOrga, "w", encoding="utf8")
	outSonstF = open(outSonst, "w", encoding="utf8")
	
	for entity in sentence.get_spans('ner'):
		tmp = str(entity.start_position) + ":" + str(entity.end_position)
		tmp += "\t" + str(entity.text).replace(" ","_") + "\t" + str(entity.get_label("ner").score)
		tmp += "\n"
		if str(entity.get_label("ner").value) == "PER":
			outPersonF.writelines(tmp)
		elif str(entity.get_label("ner").value) == "LOC":
			outOrtF.writelines(tmp)
		elif str(entity.get_label("ner").value) == "ORG":
			outOrgaF.writelines(tmp)
		else:
			outSonstF.writelines(tmp)

	outPersonF.close()
	outOrtF.close()
	outOrgaF.close()
	outSonstF.close()
	f.close()
	
	f = open("NER_done.txt", "a", encoding="utf8")
	f.write(i+"\n")
	f.close()
	
