# MDR Corona Ticker for NLP

Suggested Citation: Tiepmar, Jochen & Bräuer, Joshua Sami (2022): MDR Corona Ticker for NLP. Hannah-Arendt-Institute for the Research on Totalitarianism, Dresden. DOI: https://zenodo.org/record/6458330 .

License: CC-BY-SA

Copyright:

If you believe that copyright is violated by the publication of this dataset remember that no images are included and the original URLs are added as a source. 

# Installation 

 * Clone das Repository

 * Verschiebe den Ordner in den www-Ordner

 * Das Digilab sollte jetzt unter [host]/[ordnername] verfügbar sein.


# References

NER (Flair) 

Alan Akbik, Duncan Blythe, and Roland Vollgraf. 2018. Contextual String Embeddings for Sequence Labeling. In Proceedings of the 27th International Conference on Computational Linguistics, pages 1638–1649, Santa Fe, New Mexico, USA. Association for Computational Linguistics.

Senti_ml (BERT)

Oliver Guhr, Anne-Kathrin Schumann, Frank Bahrmann, and Hans Joachim Böhme. 2020. Training a Broad-Coverage German Sentiment Classification Model for Dialog Systems. In Proceedings of the 12th Language Resources and Evaluation Conference, pages 1627–1632, Marseille, France. European Language Resources Association.

Senti_lb (SentiWS)

R. Remus, U. Quasthoff & G. Heyer: SentiWS - a Publicly Available German-language Resource for Sentiment Analysis.
In: Proceedings of the 7th International Language Ressources and Evaluation (LREC'10), 2010

Topic Model BERT)
Maarten Grootendorst. 2022. BERTopic: Neural topic modeling with a class-based TF-IDF procedure. In arXiv preprint arXiv:2203.05794.