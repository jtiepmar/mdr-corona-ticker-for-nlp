function diagrammlink(parent)
{
	window.open(document.getElementById(parent).src, "_blank");
}

function printlink(parent)
{
	link = document.getElementById(parent).src
	if(link.includes("&print=1")){link=link.replace("&print=1","")}else{link+="&print=1"}
	document.getElementById(parent).src=link
}


function datalink(parent)
{
	window.open(dataurl+document.getElementById(parent).src.split("data=")[1].split("&")[0]+".txt", "_blank");
}


function header(){
	
str='<div  class="header">'+
'<table>'
+'<tr>'
+'<td>'
+'<a style="color:white;"  href="../../mdrcoronaticker/corpusstats/">Corpus Statistiken</a>'
+'</td>'
+'<td>'
+'Textstatistiken'
+'</td>'
+'<td>'
+'<a style="color:white;"  href="../../mdrcoronaticker/ner/">Named Entities</a>'
+'</td>'
+'<td>'
+'Sentiment Analyse'
+'</td>'

+'<td style="text-align:center;">'
+'<a style="color:white;"  href="../../mdrcoronaticker/tm/">Topic Modelle</a>'
+'</td>'

+'<td style="text-align:center;">'
+'<a style="color:white;"  href="../../mdrcoronaticker/tickerdujour">Ticker des Tages (soweit vorhanden)</a>'
+'</td>'
+'</tr>'

+'<tr >'
+'<td>'
+'</td>'
+'<td>'
+'<a style="color:white;"  href="../../mdrcoronaticker/statistics/timestats.html">Zeitserien</a> | '
+'<a style="color:white;"  href="../../mdrcoronaticker/statistics/wordstats.html">Wortstatistiken</a> | '
+'<a style="color:white;"  href="../../mdrcoronaticker/statistics/ngrams.html">nGramme</a>'

+'</td>'
+'<td>'
+'</td>'
+'<td>'
+'<a style="color:white;"  href="../../mdrcoronaticker/senti/?data=mdrcoronaticker/_all/senti&type=lb">Lexikonbasiert(SentiWS)</a> | '
+'<a style="color:white;"  href="../../mdrcoronaticker/senti/?data=mdrcoronaticker/_all/senti&type=ml">Machine Learning(BERT)</a>'

+'</td>'
+'<td>'
+'</td>'
+'<td>'
+'</td>'
+'</tr>'
+'</table>' 
+'</div>'
return str
}

function footer(){
str='<div class="header">'+
'<table>'
+'<tr>'


+'<td>'
+'<a href="../../../">Projektseite</a>'
+'</td>'
+'<td>'
+'<a href="../../../../hub/">Hub Area</a>'
+'</td>'
+'</tr>'
+'</table>' 
+'</div>'
return str
}
