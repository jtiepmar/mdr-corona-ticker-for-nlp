var diagramfont= 'Arial'
var diagramfontsize= 16
var diagramlegendfontsize= 10
var dataurl="../../../data/"
var sep="\t"
document.title = "Newsticker Text Mining"


var labelcolors = {
    "neutral":"lightgray",
    "positive":"green",
    "negative":"red"
}

document.title = ""
today = new Date();
dd = String(today.getDate()).padStart(2, '0');
mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
yyyy = today.getFullYear();
today = dd + '.' + mm + '.' + yyyy;
var wm_title = window.location.hostname+' ' +today

var watermarke = function(){
	wm = getQueryVariable("hidewatermark")
	if(wm.length==0){
		watermark = '<span style="color:gray;"> Quelle: '+wm_title+'</span>'
		return [{
		  x: 0,
		  y: 1,
		  xref: 'paper',
		  yref: 'paper',
		  xanchor:'left',
		  yanchor:'bottom',
		  text: watermark,
			font : {
			family: diagramfont,
			size : diagramfontsize,
			weight:100
			}, 
		  showarrow: false
		}]}
	else{return ""}
}


